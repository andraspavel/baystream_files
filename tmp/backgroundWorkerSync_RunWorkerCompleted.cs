        private void backgroundWorkerSync_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            notifyIcon1.BalloonTipText = "Synccycle at backgroundWorkerSync_RunWorkerCompleted";
            notifyIcon1.ShowBalloonTip(2000);

            if (syncIsEnabled)
            {
                backgroundWorkerSync.RunWorkerAsync();
            }
            else
            {
                backgroundWorkerSync.CancelAsync();
                backgroundWorkerSync.Dispose();
            }


            // Check to see if the background process was cancelled.
            if (e.Cancelled == true)
            {
                notifyIcon1.BalloonTipText = "Sync Cancelled";
                notifyIcon1.ShowBalloonTip(2000);
            }
            else if (e.Error != null)
            {
                notifyIcon1.BalloonTipText = "ERROR in Sync Execution: " + e.Error.Message; ;
                notifyIcon1.ShowBalloonTip(2000);
            }
            else
            {
                notifyIcon1.BalloonTipText = "Sync DONE!";
                notifyIcon1.ShowBalloonTip(2000);
            }



        }
