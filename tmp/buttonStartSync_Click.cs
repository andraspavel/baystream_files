 private void buttonStartSync_Click(object sender, EventArgs e)
        {
            if (!isSyncStarted)
            {
                logWriter.LogWrite("Sync Button clicked to START Syncing.");

                startSync();
            }
            else {
                logWriter.LogWrite("Sync Button clicked to STOP Syncing.");

                stopSync();
                
            }
            
        }