        private void backgroundWorkerSync_DoWork(object sender, DoWorkEventArgs e)
        {

            
            notifyIcon1.BalloonTipText = "Synccycle interval is: " + syncInterval;
            notifyIcon1.ShowBalloonTip(2000);

            //BackgroundWorker worker = sender as BackgroundWorker;

           

           /* if (this.backgroundWorkerSync.CancellationPending)
            {
                Thread.Sleep(200);
                stopSync();
                e.Cancel = true;
                return;
            }
            else
            {
                //FileSyncProvider.Main(syncLocalPath, syncRemotePath);
                //Thread.Sleep(syncInterval);
                startSync();
            }*/


            if (!isSyncStarted)
            {
                
                if (backgroundWorkerSync.CancellationPending == true)
                {
                    logWriter.LogWrite("Sync cancelled.");
                    e.Cancel = true;
                }
                else
                {
                    // Perform a time consuming operation and report progress.
                    System.Threading.Thread.Sleep(500);
                    logWriter.LogWrite("Sync starting from DoWork event.");
                    startSync();
                }
            }
            else
            {
                logWriter.LogWrite("Sync stopping from DoWork event.");
                stopSync();
               
            }

        }