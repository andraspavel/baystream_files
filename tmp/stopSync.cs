 private void stopSync()
        {
            
            //Cancelling BGworker
            if (backgroundWorkerSync.WorkerSupportsCancellation ==true)
            {
                notifyIcon1.BalloonTipText = "Stopping Sync..!";
                notifyIcon1.ShowBalloonTip(5000);

                logWriter.LogWrite("Stopping Sync..!");

                backgroundWorkerSync.CancelAsync();
                backgroundWorkerSync.Dispose();
                //trackBarSyncInterval.Enabled = true;
                Invoke(new Action(() => { trackBarSyncInterval.Enabled = true; }));

                isSyncStarted = false;
                syncIsEnabled = false;
            }
            
        }