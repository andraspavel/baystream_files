        private void startSync() {
            isSyncStarted = true;
            /*
            Invoke(new Action(() => {
                buttonSync.Text = "Stop Sync..";
                buttonSync.BackColor = Color.Yellow;
            }));
            */
            
           
            //MessageBox.Show(textBoxServerName.Text);
            //MessageBox.Show(textBoxFolderName.Text);
            //MessageBox.Show(textBoxUserName.Text);
            //MessageBox.Show(Password);
            if ((textBoxServerName.Text.Length > 3) && (textBoxFolderName.Text.Length > 3) && (textBoxUserName.Text.Length > 3) && (Password.Length > 5))
            {
                ShareNameString = String.Format("\\\\{0}\\{1}", textBoxServerName.Text, textBoxFolderName.Text);
                logWriter.LogWrite("Remote server UNC path: " + ShareNameString);
                syncRemotePath = ShareNameString;
                
                AddCMDKey(textBoxServerName.Text, textBoxFolderName.Text, Password);
            }
           
            if (!(syncRemotePath.Length > 6))
            {
                MessageBox.Show("Error: Please set up a valid remote folder!");
                logWriter.LogWrite("Error: Please set up a valid remote folder!");
                return;
            }

            if (syncLocalPath == "")
            {
                MessageBox.Show("Error: Please select a valid folder!");
                logWriter.LogWrite("Error: Please select a valid folder!");
                return;
            }
            else
            {
                //trackBarSyncInterval.Value = 2;
                trackBarSyncInterval.Enabled = false;
                string strSyncInterval = (syncInterval / 1000).ToString();
                logWriter.LogWrite("Sync Started..!");
                logWriter.LogWrite("Sync Interval is: " + strSyncInterval + " seconds.");
                logWriter.LogWrite("Local Synchronization Folder:" + syncLocalPath);
                logWriter.LogWrite("Remote Synchronization Folder:" + syncRemotePath);

                

                if (backgroundWorkerSync.IsBusy)
                {
                    notifyIcon1.BalloonTipText = "Synchronization is ALREADY in progress";
                    notifyIcon1.ShowBalloonTip(5000);
                }
                else
                {
                    syncIsEnabled = true;
                    backgroundWorkerSync.RunWorkerAsync();
                    Invoke(new Action(() => {
                        buttonSync.Text = "Stop Sync..";
                        buttonSync.BackColor = Color.Yellow;
                    }));
                    
                    notifyIcon1.BalloonTipText = "Synchronization is in progress";
                    notifyIcon1.ShowBalloonTip(5000);
                }
                
                //BSFConfigurator.BaystreamFilesConfigSave();


            }
        }


       