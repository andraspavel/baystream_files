﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Security.Permissions;
using System.Diagnostics;
using Microsoft.Synchronization;
using Microsoft.Synchronization.Files;
using System.Threading;
using System.Net.Sockets;
using System.Configuration;


namespace BaystreamFiles
{


    public partial class BaystreamFilesMainForm : Form
    {
        public static string ServerName;
        public static string FolderName;
        public static string UserName;
        public static string Password;
        public static string encryptedPassword;
        public static string LocalDrive;
        public static string ShareNameString;
        public static string syncLocalPath;
        public static string syncRemotePath;
        public static int syncInterval = 300000;
        public static bool syncIsEnabled;
        public static LogWriter logWriter = new LogWriter("Starting appplication..");
        //public static NotifyIcon notifyIconNotifictaions = new NotifyIcon();
        public static string configurationReceivedXMLFilePath;
        public static bool isSyncStarted;
        public static bool RunAsService;
        public static bool VerboseMode;
        public static Guid guid;
        public static BaystreamFilesConfigurator BSFConfigurator = new BaystreamFilesConfigurator();
        public static BaystreamFilesConfigs BSFConfig = new BaystreamFilesConfigs();
        public static NotifyIcon Notifier { get { return notifier; } }
        private static NotifyIcon notifier;
        delegate void StringArgReturningVoidDelegate(string text); //safe calls to UI controls fro BGWorker

        public BaystreamFilesMainForm()
        {

            InitializeComponent();
            
            DriveLetters();

            notifier = this.notifyIcon1;


        }



        private void DriveLetters() {

            isSyncStarted = false;
            ArrayList driveLetters = new ArrayList(26); // Allocate space for alphabet
            for (int i = 65; i < 91; i++) // increment from ASCII values for A-Z
            {
                driveLetters.Add(Convert.ToChar(i)); // Add uppercase letters to possible drive letters
            }

            foreach (string drive in Directory.GetLogicalDrives())
            {
                driveLetters.Remove(drive[0]); // removed used drive letters from possible drive letters
            }

            foreach (char drive in driveLetters)
            {
                comboBoxDriveLetters.Items.Add(drive); // add unused drive letters to the combo box
            }
        }

        private void AddCMDKey(string localServerName, string localUserName, string localPassword) {


            System.Diagnostics.Process processCMDKey = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfoCMDKey = new System.Diagnostics.ProcessStartInfo();
            startInfoCMDKey.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfoCMDKey.FileName = "cmd.exe";
            //cmdkey /add:targetname /user:username /pass:password
            string cmdKEYArgument = String.Format("\"/c cmdkey /add:{0} /user:{1} /pass:{2}", localServerName, localUserName, localPassword);
            startInfoCMDKey.Arguments = string.Format("{0}", cmdKEYArgument);
            processCMDKey.StartInfo = startInfoCMDKey;
            processCMDKey.Start();
            logWriter.LogWrite("The credentials saved successfully");
        }

        private void deleteCMDKey(string localServerName)
        {

            System.Diagnostics.Process processCMDKey = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfoCMDKey = new System.Diagnostics.ProcessStartInfo();
            startInfoCMDKey.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfoCMDKey.FileName = "cmd.exe";
            //cmdkey /add:targetname /user:username /pass:password
            string cmdKEYArgument = String.Format("\"/c cmdkey /delete:{0}", localServerName);
            //MessageBox.Show(string.Format("arguments are {0}", cmdKEYArgument), "Results", MessageBoxButtons.OK);
            startInfoCMDKey.Arguments = string.Format("{0}", cmdKEYArgument);
            processCMDKey.StartInfo = startInfoCMDKey;
            processCMDKey.Start();
            //MessageBox.Show("The credentials were deleted successfully.");
            logWriter.LogWrite("The credentials were deleted successfully.");
        }

        private void buttonQuit_Click(object sender, EventArgs e)
        {
            notifyIcon1.Visible = false;
            Application.Exit();
            logWriter.LogWrite("Exit..");
        }

        private void buttonAttach_Click(object sender, EventArgs e)
        {

            ServerName = textBoxServerName.Text;
            FolderName = textBoxFolderName.Text;
            UserName = textBoxUserName.Text;

            if (checkBoxShowPassword.Checked == false)
            {
                encryptedPassword = textBoxPassword.Text;
                Password = Encryption.Encryption.Decrypt(textBoxPassword.Text);

            }
            else
            {
                Password = textBoxPassword.Text;
            }

            if (comboBoxDriveLetters.SelectedItem == null)
            {
                MessageBox.Show("You have to select a valid letter!");
                logWriter.LogWrite("You have to select a valid letter!");
                return;
            }

            if (checkBoxPermanent.Checked)
            {

                try
                {
                    //MessageBox.Show(comboBoxDriveLetters.SelectedItem.ToString());
                    LocalDrive = String.Format("{0}:", comboBoxDriveLetters.SelectedItem.ToString());


                    ShareNameString = String.Format("\\\\{0}\\{1}", ServerName, FolderName);
                    //MessageBox.Show(String.Format("\"{0}\", \"{1}\"", UserName, Password));
                    //MessageBox.Show(ShareNameString.ToString());
                    //net use * \\hvfiles-01.baymain.com\testfolder11 /u:testuser11 P@ssw0rd01! /persistent:yes
                    string cmdArgument = String.Format("\"/c net use {0} {1} /u:{2} {3} /persistent:yes", LocalDrive, ShareNameString, UserName, Password);
                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                    startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    startInfo.FileName = "cmd.exe";
                    //MessageBox.Show(string.Format("arguments are {0}", cmdArgument), "Results", MessageBoxButtons.OK);
                    startInfo.Arguments = string.Format("{0}", cmdArgument);
                    process.StartInfo = startInfo;
                    process.Start();
                    MessageBox.Show(String.Format("The {0} path was successfully attached to {1} drive.", ShareNameString, LocalDrive));
                    logWriter.LogWrite(String.Format("The {0} path was successfully attached to {1} drive.", ShareNameString, LocalDrive));

                    if (checkBoxSaveCredentials.Checked)
                    {
                        AddCMDKey(ServerName, UserName, Password);
                    }


                }
                catch (Exception err)
                {
                    MessageBox.Show(this, "Error: " + err.Message);
                    logWriter.LogWrite("Error: " + err.Message);
                }


            }

            else
            {

                try
                {
                    //MessageBox.Show(comboBoxDriveLetters.SelectedItem.ToString());
                    LocalDrive = String.Format("{0}:", comboBoxDriveLetters.SelectedItem.ToString());


                    ShareNameString = String.Format("\\\\{0}\\{1}", ServerName, FolderName);
                    //MessageBox.Show(String.Format("\"{0}\", \"{1}\"", UserName, Password));
                    //MessageBox.Show(ShareNameString.ToString());
                    //net use * \\hvfiles-01.baymain.com\testfolder11 /u:testuser11 P@ssw0rd01!
                    //syncRemotePath = ShareNameString;
                    string cmdArgument = String.Format("\"/c net use {0} {1} /u:{2} {3}", LocalDrive, ShareNameString, UserName, Password);
                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                    startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    startInfo.FileName = "cmd.exe";
                    //MessageBox.Show(string.Format("arguments are {0}", cmdArgument), "Results", MessageBoxButtons.OK);
                    startInfo.Arguments = string.Format("{0}", cmdArgument);
                    process.StartInfo = startInfo;
                    process.Start();
                    MessageBox.Show(String.Format("The {0} path was successfully attached to {1} drive.", ShareNameString, LocalDrive));
                    logWriter.LogWrite(String.Format("The {0} path was successfully attached to {1} drive.", ShareNameString, LocalDrive));

                    if (checkBoxSaveCredentials.Checked)
                    {
                        AddCMDKey(ServerName, UserName, Password);
                    }

                }
                catch (Exception err)
                {
                    MessageBox.Show(this, "ERROR: " + err.Message);
                    logWriter.LogWrite("ERROR: " + err.Message);
                }

            }

        }

        private void Detach_Click(object sender, EventArgs e)
        {
            
            try
            {

                //MessageBox.Show(comboBoxDriveLetters.SelectedItem.ToString());
                if (!(LocalDrive.Length > 0))
                {
                    LocalDrive = String.Format("{0}:", comboBoxDriveLetters.SelectedItem.ToString());
                }



                ShareNameString = String.Format("\\\\{0}\\{1}", ServerName, FolderName);
                //MessageBox.Show(String.Format("\"{0}\", \"{1}\"", UserName, Password));
                //MessageBox.Show(ShareNameString.ToString());
                //net use * \\hvfiles-01.baymain.com\testfolder11 /u:testuser11 P@ssw0rd01!
                string cmdArgument = String.Format("\"/c net use {0} /delete", LocalDrive);
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                //MessageBox.Show(string.Format("arguments are {0}", cmdArgument), "Results", MessageBoxButtons.OK);
                startInfo.Arguments = string.Format("{0}", cmdArgument);
                process.StartInfo = startInfo;
                process.Start();
                MessageBox.Show(String.Format("The {0} path was successfully detached from {1} drive.", ShareNameString, LocalDrive));
                logWriter.LogWrite(String.Format("The {0} path was successfully detached from {1} drive.", ShareNameString, LocalDrive));

                if (checkBoxSaveCredentials.Checked)
                {
                    deleteCMDKey(ServerName);
                }


            }
            catch (Exception err)
            {
                MessageBox.Show(this, "Error: " + err.Message);
                logWriter.LogWrite("Error: " + err.Message);
            }

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void buttonBrowseXML_Click(object sender, EventArgs e)
        {
            Stream configXMLstream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.Filter = "XML files (*.xml)|*.xml";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                try
                {
                    if ((configXMLstream = openFileDialog1.OpenFile()) != null)
                    {

                        using (configXMLstream)
                        {
                            // Insert code to read the stream here.
                            //MessageBox.Show(myStream.ToString());
                            XmlDocument parsedmyXMLstream = new XmlDocument();
                            parsedmyXMLstream.Load(configXMLstream);
                            //MessageBox.Show(parsedMyStream.SelectSingleNode("FileShare/User/LoginAccount/Username").InnerText);
                            //MessageBox.Show(parsedMyStream.SelectSingleNode("FileShare/User/LoginAccount/Password").InnerText);
                            //MessageBox.Show(parsedMyStream.SelectSingleNode("FileShare/User/LoginAccount/Email").InnerText);
                            //MessageBox.Show(parsedMyStream.SelectSingleNode("FileShare/Folder/Name").InnerText);
                            //MessageBox.Show(parsedMyStream.SelectSingleNode("FileShare/Folder/ServerName").InnerText);
                            logWriter.LogWrite("XML parsed..");

                            if (parsedmyXMLstream.SelectSingleNode("FileShare/Folder/ServerName").InnerText == "")
                            {
                                MessageBox.Show("Please select a valid XML file!");
                                logWriter.LogWrite("Please select a valid XML file!");


                                return;
                            }

                            textBoxServerName.Text = parsedmyXMLstream.SelectSingleNode("FileShare/Folder/ServerName").InnerText;
                            textBoxFolderName.Text = parsedmyXMLstream.SelectSingleNode("FileShare/Folder/Name").InnerText;
                            textBoxUserName.Text = parsedmyXMLstream.SelectSingleNode("FileShare/User/LoginAccount/Username").InnerText;
                            textBoxPassword.Text = parsedmyXMLstream.SelectSingleNode("FileShare/User/LoginAccount/Password").InnerText;

                            buttonAttach.Enabled = true;
                            buttonDetach.Enabled = true;
                            buttonTestConnection.Enabled = true;
                            ServerName = textBoxServerName.Text;
                            buttonMFilesServerConfig.Enabled = true;
                            configurationReceivedXMLFilePath = openFileDialog1.FileName.ToString();

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Concat("ERROR: Please select a valid XML file! Error Message: {0}", ex));
                    logWriter.LogWrite(string.Concat("ERROR: Please select a valid XML file! Error Message: {0}", ex));
                }
            }


        }

        private void linkLabelSupport_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://support.baymain.com");

        }

        private void checkBoxShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxShowPassword.Checked == false)
            {

                string deCryptedPassword = Encryption.Encryption.Encrypt(textBoxPassword.Text);
                textBoxPassword.Text = deCryptedPassword.ToString();
                textBoxPassword.UseSystemPasswordChar = true;

            }
            else
            {

                string plainPassword = Encryption.Encryption.Decrypt(textBoxPassword.Text);
                textBoxPassword.Text = plainPassword.ToString();
                textBoxPassword.UseSystemPasswordChar = false;

            }

        }

        private void buttonBrowseLocalFolder_Click(object sender, EventArgs e)
        {

            FolderBrowserDialog openFileDialogLocalFolder = new FolderBrowserDialog();
            openFileDialogLocalFolder.ShowNewFolderButton = true;
            openFileDialogLocalFolder.RootFolder = Environment.SpecialFolder.MyComputer;

            if (openFileDialogLocalFolder.ShowDialog() == DialogResult.OK)
            {
                syncLocalPath = openFileDialogLocalFolder.SelectedPath;
                textBoxLocalFolderName.Text = openFileDialogLocalFolder.SelectedPath;
                buttonSync.Enabled = true;
                buttonSync.BackColor = Color.LightGreen;
                trackBarSyncInterval.Enabled = true;
                labelTrackBarValues.Text = "1 minute";
                labelTrackBarValues.Visible = true;
                logWriter.LogWrite("Local Sync Folder is : " + openFileDialogLocalFolder.SelectedPath);

            }
            else
            {
                MessageBox.Show("Error: Please select a valid folder!");
                logWriter.LogWrite("Error: Please select a valid folder!");
                textBoxLocalFolderName.Text = "";
            }




        }

        private void syncIntervalLabelConfiguration()
        {
            switch (trackBarSyncInterval.Value)
            {
                case 1:
                    labelTrackBarValues.Text = "1 minute";
                    syncInterval = 60000;
                    break;
                case 2:
                    labelTrackBarValues.Text = "5 minute";
                    syncInterval = 300000;
                    break;
                case 3:
                    labelTrackBarValues.Text = "15 minutes";
                    syncInterval = 900000;
                    break;
                case 4:
                    labelTrackBarValues.Text = "30 minutes";
                    syncInterval = 1800000;
                    break;
                case 5:
                    labelTrackBarValues.Text = "1 hour";
                    syncInterval = 3600000;
                    break;
                case 6:
                    labelTrackBarValues.Text = "2 hours";
                    syncInterval = 7200000;
                    break;
                case 7:
                    labelTrackBarValues.Text = "4 hours";
                    syncInterval = 14400000;
                    break;
                case 8:
                    labelTrackBarValues.Text = "8 hours";
                    syncInterval = 28800000;
                    break;
                case 9:
                    labelTrackBarValues.Text = "1 day";
                    syncInterval = 86400000;
                    break;
                default:
                    labelTrackBarValues.Text = "Select interval please."; ;
                    break;

            }
            labelTrackBarValues.Visible = true;
        }

        private void trackBarSyncInterval_Scroll(object sender, EventArgs e)
        {

            syncIntervalLabelConfiguration();

        }

        public void startSync() {

            buttonStopSync.Enabled = true;
            buttonSync.Enabled = false;

            if (backgroundWorkerSync.CancellationPending == true)
            {
                logWriter.LogWrite("Synchronization Cancelled");
                MessageBox.Show("Stopping earlier synchronizer process. Please try again after the synchronization is completed.");
                logWriter.LogWrite("Stopping earlier synchronizer process. Please try again after the synchronization is completed.");
                isSyncStarted = false;
                timerSyncInterval.Stop();
                timerSyncInterval.Enabled = false;
                stopSync();

            }
            else
            {
                // Perform a time consuming operation and report progress.
                //System.Threading.Thread.Sleep(500);
                logWriter.LogWrite("Starting up synchronizer process.");

                if ((textBoxServerName.Text.Length > 3) && (textBoxFolderName.Text.Length > 3) && (textBoxUserName.Text.Length > 3) && (Password.Length > 5))
                {
                    syncRemotePath = String.Format("\\\\{0}\\{1}", textBoxServerName.Text, textBoxFolderName.Text);
                    logWriter.LogWrite("Remote server UNC path: " + syncRemotePath);
                    //syncRemotePath = ShareNameString;

                    AddCMDKey(textBoxServerName.Text, textBoxUserName.Text, Password);
                }

                if (!(syncRemotePath.Length > 6))
                {
                    MessageBox.Show("Error: Please set up a valid remote folder!");
                    logWriter.LogWrite("Error: Please set up a valid remote folder!");
                    return;
                }

                if (syncLocalPath == "")
                {
                    MessageBox.Show("Error: Please select a valid folder!");
                    logWriter.LogWrite("Error: Please select a valid folder!");
                    return;
                }
                else
                { string strSyncInterval = (syncInterval / 1000).ToString();

                    //MessageBox.Show(syncInterval.ToString());
                    syncIsEnabled = true;
                    timerSyncInterval.Enabled = true;
                    timerSyncInterval_Tick(null, null);
                    timerSyncInterval.Interval = syncInterval;
                    timerSyncInterval.Start();
                    logWriter.LogWrite("Sync Started..!");
                    logWriter.LogWrite("Sync Interval is: " + strSyncInterval + " seconds.");
                    logWriter.LogWrite("Local Synchronization Folder:" + syncLocalPath);
                    logWriter.LogWrite("Remote Synchronization Folder:" + syncRemotePath);

                }
            }
        }

        public void stopSync()
        {

            trackBarSyncInterval.Enabled = true;
            syncIsEnabled = false;
            buttonSync.Enabled = true;
            buttonStopSync.Enabled = false;
            backgroundWorkerSync.CancelAsync();
            isSyncStarted = false;
            timerSyncInterval.Stop();
            timerSyncInterval.Enabled = false;
        }

        private void buttonStartSync_Click(object sender, EventArgs e)
        {

            startSync();

        }
        private void buttonStopSync_Click(object sender, EventArgs e)
        {
            stopSync();
        }

        public void buttonViewLog_Click(object sender, EventArgs e)
        {
            string logFilePath = Application.StartupPath + "\\" + "log.txt";
            Process.Start(logFilePath);

        }

        private void backgroundWorkerSync_DoWork(object sender, DoWorkEventArgs e)
        {
            //logWriter.LogWrite(syncLocalPath);
            //logWriter.LogWrite(syncRemotePath);
            FileSyncProvider.Main(syncLocalPath, syncRemotePath);
        }

        private void backgroundWorkerSync_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Check to see if the background process was cancelled.
            if (e.Cancelled == true)
            {
                syncIsEnabled = false;
                notifyIcon1.BalloonTipText = "Synchronization Cancelled. Finishing current executions.";
                notifyIcon1.ShowBalloonTip(2000);
                logWriter.LogWrite("Synchronization Cancelled. Finishing current executions.");
                backgroundWorkerSync.CancelAsync();
                backgroundWorkerSync.Dispose();
                isSyncStarted = false;


            }
            else if (e.Error != null)
            {
                notifyIcon1.BalloonTipText = "ERROR in Sync Execution: " + e.Error.Message; 
                notifyIcon1.ShowBalloonTip(2000);
                logWriter.LogWrite(string.Format("ERROR in Sync Execution: " + e.Error.Message));
                syncIsEnabled = false;
                isSyncStarted = false;
            }
            else
            {
                syncIsEnabled = true;
                //notifyIcon1.BalloonTipText = "Sync DONE!";
                //notifyIcon1.ShowBalloonTip(2000);
                isSyncStarted = true;
                timerSyncInterval.Enabled = true;
                timerSyncInterval.Start();

            }
        }

        private void timerSyncInterval_Tick(object sender, EventArgs e)
        {
            Thread.Sleep(5000);
            timerSyncInterval.Enabled = false;
            isSyncStarted = true;
            logWriter.LogWrite("Tick..");
            if (!backgroundWorkerSync.IsBusy)
            {
                backgroundWorkerSync.RunWorkerAsync();
            }

        }

        private void buttonTestConnection_Click(object sender, EventArgs e)
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                try
                {
                    tcpClient.ReceiveTimeout = 3000;
                    tcpClient.SendTimeout = 3000;
                    tcpClient.Connect(ServerName, 445);
                    logWriter.LogWrite("Connection Test: Success: The connection to[" + ServerName + "] is available!");
                    MessageBox.Show("Connection Test: Success: The connection to [" + ServerName + "] is available!");
                }
                catch (Exception)
                {
                    logWriter.LogWrite("Connection Test: ERROR: The [" + ServerName + "] server is not avaiable!please check network connection!");
                    MessageBox.Show("ERROR: The [" + ServerName + "] server is not avaiable! please check network connection!");
                }
            }
        }

        private void buttonMFilesServerConfig_Click(object sender, EventArgs e)
        {

            /* TaskService ts = new TaskService();

             // Create a new task definition and assign properties
             TaskDefinition td = ts.NewTask();
             td.RegistrationInfo.Description = "Configure M-Fles Server with additional credentials";

             // Create a trigger that will fire the task at this time every other day
             td.Triggers.Add(new DailyTrigger { DaysInterval = 2 });

             // Create an action that will launch Notepad whenever the trigger fires
             //ServerName, UserName, Password
             string cmdKEYArgument = String.Format("/add:{0} /user:{1} /pass:{2}", ServerName, UserName, Password);
             td.Actions.Add(new ExecAction("cmdkey.exe", cmdKEYArgument, null));
             td.Principal.UserId = "SYSTEM";    
             td.Principal.RunLevel = TaskRunLevel.Highest;

         // Register the task in the root folder
             ts.RootFolder.RegisterTaskDefinition(@"BaystreamTasks", td);
             */

            //SchTasks /Create /SC DAILY /TN “My Task” /TR “C:RunMe.bat” /ST 09:00 /RU SYSTEM
            //string taskSchedulerArgument1 = String.Format("SchTasks /Create /SC DAILY /TN \"BaystreamTasks\" /TR \"cmdkey /add:{0} /user:{1} /pass:{2} \" /ST 01:00 /RU SYSTEM", ServerName, UserName, Password);
            StringBuilder longArgument = new StringBuilder();
            logWriter.LogWrite("M-Files Server Service configuration: Building configuration batch");
            longArgument.AppendLine("SchTasks /Delete /TN \"BaystreamTasks\" /F");
            //longArgument.AppendLine("pause");
            longArgument.AppendLine(String.Format("SchTasks /Create /SC DAILY /TN \"BaystreamTasks\" /TR \"cmdkey /add:{0} /user:{1} /pass:{2}\" /ST 01:00 /RU SYSTEM", ServerName, UserName, Password));
            longArgument.AppendLine("SchTasks /Run /TN \"BaystreamTasks\" /I");
            longArgument.AppendLine("SchTasks /Delete /TN \"BaystreamTasks\" /F");
            logWriter.LogWrite("M-Files Server Service configuration: File content prepared, writing to temporary file");
            string tempFolderPath = Path.GetTempPath();
            System.IO.StreamWriter tempFile = new System.IO.StreamWriter(tempFolderPath + "BS-tempCommandCMDKey.bat");
            string tempFileFullPath = tempFolderPath + "BS-tempCommandCMDKey.bat";
            //MessageBox.Show(tempFileFullPath);
            //write to file using streamWriter
            tempFile.Write(longArgument.ToString());
            tempFile.Dispose();
            logWriter.LogWrite(string.Format("M-Files Server Service configuration: Temporary file created on the following path: {0}", tempFileFullPath));
            //            MessageBox.Show(longArgument.ToString());
            //System.IO.StreamReader tempFileRead = new System.IO.StreamReader(tempFolderPath + "BS-tempCommandCMDKey.bat");
            //var tempFileConent = tempFileRead.ReadToEnd();
            //MessageBox.Show(tempFileConent.ToString());


            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Verb = "runas";
                //startInfo.Arguments = string.Format("{0}", longArgument);
                startInfo.Arguments = string.Format("/c {0}", tempFileFullPath);
                process.StartInfo = startInfo;
                process.Start();
                Thread.Sleep(200);
                File.Delete(tempFileFullPath);
                logWriter.LogWrite(string.Format("M-Files Server Service configuration: Temporary file deleted"));
            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR: M-Files Server Service configuration: Failed to add the credentials to M-Files Server Service. Please contact Baystream Support.");
                logWriter.LogWrite("ERROR: M-Files Server Service configuration: Failed to add the credentials to M-Files Server Service. Please contact Baystream Support. Message: " + ex);
            }

        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
                notifyIcon1.Visible = true;
            }
        }

        private void NotifyIcon1_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
            }

            //Set entries in config file
            BSFConfig.ConfigurationXMLfile = BaystreamFilesConfigs.ConfigFilePath;
            //BSFConfig.DestinationDriveLetter = LocalDrive;
            BSFConfig.DestinationHotFolderName = textBoxFolderName.Text.ToString();
            BSFConfig.DestinationIsCredentialsSaved = checkBoxSaveCredentials.Checked;
            BSFConfig.DestinationIsPasswordShow = checkBoxShowPassword.Checked;
            BSFConfig.DestinationIsPermanent = checkBoxPermanent.Checked;
            BSFConfig.DestinationPassword = textBoxPassword.Text.ToString();
            BSFConfig.DestinationServerName = textBoxServerName.Text.ToString();
            BSFConfig.DestinationSyncInterval = trackBarSyncInterval.Value;
            BSFConfig.DestinationUserName = textBoxUserName.Text.ToString();
            BSFConfig.SourceLocalFolder = syncLocalPath;
            BSFConfig.SourceSyncIsEnabled = syncIsEnabled;
            BSFConfig.SourceSyncIsStarted = isSyncStarted;
            BSFConfig.VerboseMode = VerboseMode;
            BSFConfig.guid = guid;

            BSFConfigurator.BaystreamFilesConfigSave();


        }

        private void Form1_Load(object sender, EventArgs e)
        {

            /*try
            {
            //Serialize xml file to get last config
            BSFConfigurator.BaystreamFilesConfigLoad();
            }
            catch (Exception)
            {
                logWriter.LogWrite("Info: Loading data from configuration file failed. Creating new configuration file.");
                //guid = ((BaystreamFilesConfigurator.BSFConfigonLoad.guid).ToString() == "00000000-0000-0000-0000-000000000000") ? Guid.NewGuid() : BaystreamFilesConfigurator.BSFConfigonLoad.guid;
            }
            */
            BSFConfigurator.BaystreamFilesConfigLoad();


            logWriter.LogWrite("Loading data from configuration file");
            //Load entries from config file   
            try
            {


                //LocalDrive = ((BaystreamFilesConfigurator.BSFConfigonLoad.DestinationDriveLetter).Length > 0) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationDriveLetter : null;
                textBoxFolderName.Text = ((BaystreamFilesConfigurator.BSFConfigonLoad.DestinationHotFolderName).Length > 4) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationHotFolderName : null;
                checkBoxSaveCredentials.Checked = (BaystreamFilesConfigurator.BSFConfigonLoad.DestinationIsCredentialsSaved) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationIsCredentialsSaved : false;
                checkBoxShowPassword.Checked = (BaystreamFilesConfigurator.BSFConfigonLoad.DestinationIsPasswordShow) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationIsPasswordShow : false;
                checkBoxPermanent.Checked = (BaystreamFilesConfigurator.BSFConfigonLoad.DestinationIsPermanent) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationIsPermanent : false;
                textBoxPassword.Text = ((BaystreamFilesConfigurator.BSFConfigonLoad.DestinationPassword).Length > 10) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationPassword : null;
                textBoxServerName.Text = ((BaystreamFilesConfigurator.BSFConfigonLoad.DestinationServerName).Length > 5) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationServerName : null;
                trackBarSyncInterval.Value = (BaystreamFilesConfigurator.BSFConfigonLoad.DestinationSyncInterval > 0) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationSyncInterval : 1;
                textBoxUserName.Text = ((BaystreamFilesConfigurator.BSFConfigonLoad.DestinationUserName).Length > 3) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationUserName : null;
                syncLocalPath = ((BaystreamFilesConfigurator.BSFConfigonLoad.SourceLocalFolder).Length > 3) ? BaystreamFilesConfigurator.BSFConfigonLoad.SourceLocalFolder : "";
                textBoxLocalFolderName.Text = ((BaystreamFilesConfigurator.BSFConfigonLoad.SourceLocalFolder).Length > 0) ? BaystreamFilesConfigurator.BSFConfigonLoad.SourceLocalFolder : "";
                syncIsEnabled = (BaystreamFilesConfigurator.BSFConfigonLoad.SourceSyncIsEnabled) ? BaystreamFilesConfigurator.BSFConfigonLoad.SourceSyncIsEnabled : false;
                isSyncStarted = (BaystreamFilesConfigurator.BSFConfigonLoad.SourceSyncIsStarted) ? BaystreamFilesConfigurator.BSFConfigonLoad.SourceSyncIsStarted : false;
                //RunAsService = (BaystreamFilesConfigurator.BSFConfigonLoad.RunAsService) ? BaystreamFilesConfigurator.BSFConfigonLoad.RunAsService : false;
                VerboseMode = (BaystreamFilesConfigurator.BSFConfigonLoad.VerboseMode) ? BaystreamFilesConfigurator.BSFConfigonLoad.VerboseMode : false;
                //guid = ((BaystreamFilesConfigurator.BSFConfigonLoad.guid).ToString() == "00000000-0000-0000-0000-000000000000") ? Guid.NewGuid()  : BaystreamFilesConfigurator.BSFConfigonLoad.guid;

                if (textBoxPassword.Text.Length > 20)
                {
                    encryptedPassword = textBoxPassword.Text;
                    Password = Encryption.Encryption.Decrypt(textBoxPassword.Text);
                }

                if ((trackBarSyncInterval.Value > 0) && (syncLocalPath.Length > 3) && (syncIsEnabled))
                {
                    //MessageBox.Show("Trackbarvalue >0 and synclocalpath is not null");
                    syncIntervalLabelConfiguration();
                    startSync();

                }
                if ((trackBarSyncInterval.Value > 0) && (syncLocalPath.Length > 3) && (!(syncIsEnabled)) && (textBoxFolderName.Text.Length > 3))
                {
                    buttonSync.Enabled = true;
                    trackBarSyncInterval.Enabled = true;
                    syncIntervalLabelConfiguration();
                    buttonAttach.Enabled = true;

                    buttonTestConnection.Enabled = true;

                }

                if (textBoxServerName.Text.Length > 5)
                {
                    ServerName = textBoxServerName.Text;
                }
                if (textBoxUserName.Text.Length > 3)
                {
                    UserName = textBoxUserName.Text;
                }
                //MessageBox.Show(LocalDrive);
                if ((LocalDrive).Length > 0)
                {
                    /*
                    foreach (char drive in comboBoxDriveLetters.Items)
                    {

                    if (drive.ToString() == LocalDrive)
                    {
                        comboBoxDriveLetters.SelectedItem = LocalDrive;
                        comboBoxDriveLetters.Text = LocalDrive;
                    }

                    }
                    */
                    comboBoxDriveLetters.SelectedItem = LocalDrive;
                    comboBoxDriveLetters.Text = LocalDrive;
                    buttonDetach.Enabled = true;


                }
            }
            catch (Exception)
            {
                logWriter.LogWrite("ERROR: Configuration cannot be read. Starting with empty configuration!");
            }
            //MessageBox.Show(guid.ToString());

            try
            {
                guid = ((BaystreamFilesConfigurator.BSFConfigonLoad.guid).ToString() == "00000000-0000-0000-0000-000000000000") ? Guid.NewGuid() : BaystreamFilesConfigurator.BSFConfigonLoad.guid;

                if (guid.ToString().Contains("00000000-0000-0000-0000-000000000000") || guid.ToString().Length < 10)
                {
                    logWriter.LogWrite("ERROR: Installation GUID is not valid! Stopping service.");
                    
                }
                else
                {

                    if (VerboseMode)
                    {
                        logWriter.LogWrite("VERBOSE: Installation GUID is: " + guid);
                    }
                }
            }
            catch (Exception ex)
            {
                logWriter.LogWrite("ERROR: Installation GUID cannot be retreived. Error Message: " + ex.Message);
                
            }


            //MessageBox.Show(syncIsEnabled.ToString());
            
            }
           

        

        private void buttonSharingOptions_Click(object sender, EventArgs e)
        {

        }

        private void buttonCreateService_Click(object sender, EventArgs e)
        {

        }

        private void checkBoxVerboseMode_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxVerboseMode.Checked == false)
            {
                VerboseMode = true;

            }
            else
            {
                VerboseMode = false;
            }
        }
    }

  
    }

