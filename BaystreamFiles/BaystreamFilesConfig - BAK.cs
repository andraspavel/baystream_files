﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace BaystreamFiles
{
   public class BaystreamFilesConfig : ApplicationSettingsBase
    {
        
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string ConfigurationXMLfilePath
        {
            get
            {
                return ((string)(this["ConfigurationXMLfilePath"]));
            }
            set
            {
                this["ConfigurationXMLfilePath"] = value;
            }
        }
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string destinationUserName
        {
            get
            {
                return ((string)(this["destinationUserName"]));
            }
            set
            {
                this["destinationUserName"] = value;
            }
        }
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]

        public string destinationPassword
        {
            get
            {
                return ((string)(this["destinationPassword"]));
            }
            set
            {
                this["destinationPassword"] = value;
            }
        }
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string destinationDriveLetter
        {
            get
            {
                return ((string)(this["destinationDriveLetter"]));
            }
            set
            {
                this["destinationDriveLetter"] = value;
            }
        }
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string sourceLocalFolder
        {
            get
            {
                return ((string)(this["sourceLocalFolder"]));
            }
            set
            {
                this["sourceLocalFolder"] = value;
            }
        }
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public bool destinationSyncIsEnabled
        {
            get
            {
                return ((bool)(this["destinationSyncIsEnabled"]));
            }
            set
            {
                this["destinationSyncIsEnabled"] = value;
            }
        }
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public int destinationSyncInterval
        {
            get
            {
                return ((int)(this["destinationSyncInterval"]));
            }
            set
            {
                this["destinationSyncInterval"] = value;
            }
        }
        public void ReadConfiguration()
        {
            try
            {
                string CurrentPath = this.GetType().Assembly.Location;
                ConfigurationXMLfilePath = CurrentPath + ".config";
                this.SettingsKey = "BaystreamFiles";
                




            }
            catch (Exception ex)
            {
                string readConfigurationError = ex.Message;
            }
        }
        public void WriteConfiguration()
            {
                try
                {
                string CurrentPath = this.GetType().Assembly.Location;
                ConfigurationXMLfilePath = CurrentPath + ".config";
                this.SettingsKey = "BaystreamFiles";
                




            }
            catch (Exception ex)
                {
                    string writeConfigurationError = ex.Message;
                }
            }
    }
}
