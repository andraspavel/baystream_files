﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
namespace Encryption
{
    public  class Encryption
    {
        private static string salt = "2UN5FbxsWUezTQqWeeFwwiSEquaaifLb72t+fNybyhMg5vedw5Fe9OyG9RqTzl/w5S3WCHUuZuogTgVN/sg9BvL5WdQc7N89iNC0Dzn2BzriAv4ClfQZMHXk99j9xjKTV+LMPggz28T9CID+GiR0VAIx5ofU7wr8SUb+puBBeEs";

        public static string Encrypt(string clearText)
        {
            return Encrypt( clearText, salt);
        }

        public static string Encrypt(string clearText, string salt)
        {

            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(salt, new byte[] {
                0x52,
                0x6f,
                0x63,
                0x6b,
                0x20,
                0x61,
                0x6e,
                0x64,
                0x20,
                0x52,
                0x6f,
                0x6c,
                0x6c,
                0x21
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        //
        public static string Decrypt(string cipherText)
        {
            return Decrypt(cipherText, salt);
        }
        public static string Decrypt(string cipherText, string EncryptionKey)
        {
            
            byte[] cipherBytes;
            try
            {
                cipherBytes = Convert.FromBase64String(cipherText);
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            using (Aes encryptor = Aes.Create())
            {
                //
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
                0x52,
                0x6f,
                0x63,
                0x6b,
                0x20,
                0x61,
                0x6e,
                0x64,
                0x20,
                0x52,
                0x6f,
                0x6c,
                0x6c,
                0x21
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    }

}
