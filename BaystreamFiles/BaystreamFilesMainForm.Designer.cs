﻿namespace BaystreamFiles
{
    partial class BaystreamFilesMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaystreamFilesMainForm));
            this.buttonAttach = new System.Windows.Forms.Button();
            this.buttonDetach = new System.Windows.Forms.Button();
            this.checkBoxPermanent = new System.Windows.Forms.CheckBox();
            this.labelServerName = new System.Windows.Forms.Label();
            this.textBoxServerName = new System.Windows.Forms.TextBox();
            this.labelUsername = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.labelPermanent = new System.Windows.Forms.Label();
            this.buttonQuit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxFolderName = new System.Windows.Forms.TextBox();
            this.labelDriveLetter = new System.Windows.Forms.Label();
            this.comboBoxDriveLetters = new System.Windows.Forms.ComboBox();
            this.buttonBrowseXML = new System.Windows.Forms.Button();
            this.linkLabelSupport = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxSaveCredentials = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxShowPassword = new System.Windows.Forms.CheckBox();
            this.labelShowPassword = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxLocalFolderName = new System.Windows.Forms.TextBox();
            this.buttonBrowseLocalFolder = new System.Windows.Forms.Button();
            this.labelInterval = new System.Windows.Forms.Label();
            this.buttonSync = new System.Windows.Forms.Button();
            this.trackBarSyncInterval = new System.Windows.Forms.TrackBar();
            this.labelTrackBarValues = new System.Windows.Forms.Label();
            this.buttonViewLog = new System.Windows.Forms.Button();
            this.backgroundWorkerSync = new System.ComponentModel.BackgroundWorker();
            this.buttonMFilesServerConfig = new System.Windows.Forms.Button();
            this.buttonCreateService = new System.Windows.Forms.Button();
            this.buttonTestConnection = new System.Windows.Forms.Button();
            this.buttonSharingOptions = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.buttonStopSync = new System.Windows.Forms.Button();
            this.timerSyncInterval = new System.Windows.Forms.Timer(this.components);
            this.checkBoxVerboseMode = new System.Windows.Forms.CheckBox();
            this.labelVerbose = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSyncInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAttach
            // 
            this.buttonAttach.Enabled = false;
            this.buttonAttach.Location = new System.Drawing.Point(120, 241);
            this.buttonAttach.Name = "buttonAttach";
            this.buttonAttach.Size = new System.Drawing.Size(99, 23);
            this.buttonAttach.TabIndex = 2;
            this.buttonAttach.Text = "Attach";
            this.buttonAttach.UseVisualStyleBackColor = true;
            this.buttonAttach.Click += new System.EventHandler(this.buttonAttach_Click);
            // 
            // buttonDetach
            // 
            this.buttonDetach.Enabled = false;
            this.buttonDetach.Location = new System.Drawing.Point(225, 241);
            this.buttonDetach.Name = "buttonDetach";
            this.buttonDetach.Size = new System.Drawing.Size(99, 23);
            this.buttonDetach.TabIndex = 3;
            this.buttonDetach.Text = "Detach";
            this.buttonDetach.UseVisualStyleBackColor = true;
            this.buttonDetach.Click += new System.EventHandler(this.Detach_Click);
            // 
            // checkBoxPermanent
            // 
            this.checkBoxPermanent.AutoSize = true;
            this.checkBoxPermanent.Checked = true;
            this.checkBoxPermanent.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPermanent.Location = new System.Drawing.Point(166, 158);
            this.checkBoxPermanent.Name = "checkBoxPermanent";
            this.checkBoxPermanent.Size = new System.Drawing.Size(44, 17);
            this.checkBoxPermanent.TabIndex = 8;
            this.checkBoxPermanent.Text = "Yes";
            this.checkBoxPermanent.UseVisualStyleBackColor = true;
            // 
            // labelServerName
            // 
            this.labelServerName.AutoSize = true;
            this.labelServerName.Location = new System.Drawing.Point(12, 46);
            this.labelServerName.Name = "labelServerName";
            this.labelServerName.Size = new System.Drawing.Size(123, 13);
            this.labelServerName.TabIndex = 3;
            this.labelServerName.Text = "Destination Server DNS:";
            // 
            // textBoxServerName
            // 
            this.textBoxServerName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBoxServerName.Location = new System.Drawing.Point(166, 43);
            this.textBoxServerName.Name = "textBoxServerName";
            this.textBoxServerName.ReadOnly = true;
            this.textBoxServerName.Size = new System.Drawing.Size(479, 20);
            this.textBoxServerName.TabIndex = 4;
            // 
            // labelUsername
            // 
            this.labelUsername.AutoSize = true;
            this.labelUsername.Location = new System.Drawing.Point(13, 102);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Size = new System.Drawing.Size(114, 13);
            this.labelUsername.TabIndex = 5;
            this.labelUsername.Text = "Destination Username:";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(13, 132);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(112, 13);
            this.labelPassword.TabIndex = 6;
            this.labelPassword.Text = "Destination Password:";
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBoxUserName.Location = new System.Drawing.Point(166, 99);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.ReadOnly = true;
            this.textBoxUserName.Size = new System.Drawing.Size(479, 20);
            this.textBoxUserName.TabIndex = 6;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBoxPassword.Location = new System.Drawing.Point(166, 130);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.ReadOnly = true;
            this.textBoxPassword.Size = new System.Drawing.Size(480, 20);
            this.textBoxPassword.TabIndex = 7;
            this.textBoxPassword.UseSystemPasswordChar = true;
            // 
            // labelPermanent
            // 
            this.labelPermanent.AutoSize = true;
            this.labelPermanent.Location = new System.Drawing.Point(13, 159);
            this.labelPermanent.Name = "labelPermanent";
            this.labelPermanent.Size = new System.Drawing.Size(64, 13);
            this.labelPermanent.TabIndex = 10;
            this.labelPermanent.Text = "Permanent?";
            // 
            // buttonQuit
            // 
            this.buttonQuit.Location = new System.Drawing.Point(545, 380);
            this.buttonQuit.Name = "buttonQuit";
            this.buttonQuit.Size = new System.Drawing.Size(99, 23);
            this.buttonQuit.TabIndex = 10;
            this.buttonQuit.Text = "Quit";
            this.buttonQuit.UseVisualStyleBackColor = true;
            this.buttonQuit.Click += new System.EventHandler(this.buttonQuit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Destination Hot-Folder Name:";
            // 
            // textBoxFolderName
            // 
            this.textBoxFolderName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBoxFolderName.Location = new System.Drawing.Point(166, 71);
            this.textBoxFolderName.Name = "textBoxFolderName";
            this.textBoxFolderName.ReadOnly = true;
            this.textBoxFolderName.Size = new System.Drawing.Size(479, 20);
            this.textBoxFolderName.TabIndex = 5;
            // 
            // labelDriveLetter
            // 
            this.labelDriveLetter.AutoSize = true;
            this.labelDriveLetter.Location = new System.Drawing.Point(12, 208);
            this.labelDriveLetter.Name = "labelDriveLetter";
            this.labelDriveLetter.Size = new System.Drawing.Size(65, 13);
            this.labelDriveLetter.TabIndex = 14;
            this.labelDriveLetter.Text = "Drive Letter:";
            // 
            // comboBoxDriveLetters
            // 
            this.comboBoxDriveLetters.FormattingEnabled = true;
            this.comboBoxDriveLetters.Location = new System.Drawing.Point(166, 205);
            this.comboBoxDriveLetters.Name = "comboBoxDriveLetters";
            this.comboBoxDriveLetters.Size = new System.Drawing.Size(44, 21);
            this.comboBoxDriveLetters.TabIndex = 9;
            // 
            // buttonBrowseXML
            // 
            this.buttonBrowseXML.Location = new System.Drawing.Point(15, 241);
            this.buttonBrowseXML.Name = "buttonBrowseXML";
            this.buttonBrowseXML.Size = new System.Drawing.Size(99, 23);
            this.buttonBrowseXML.TabIndex = 1;
            this.buttonBrowseXML.Text = "Browse for XML File";
            this.buttonBrowseXML.UseVisualStyleBackColor = true;
            this.buttonBrowseXML.Click += new System.EventHandler(this.buttonBrowseXML_Click);
            // 
            // linkLabelSupport
            // 
            this.linkLabelSupport.ActiveLinkColor = System.Drawing.Color.Blue;
            this.linkLabelSupport.AutoSize = true;
            this.linkLabelSupport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelSupport.Location = new System.Drawing.Point(563, 16);
            this.linkLabelSupport.Name = "linkLabelSupport";
            this.linkLabelSupport.Size = new System.Drawing.Size(82, 16);
            this.linkLabelSupport.TabIndex = 15;
            this.linkLabelSupport.TabStop = true;
            this.linkLabelSupport.Text = "Get Support!";
            this.linkLabelSupport.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelSupport_LinkClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Save Credentials?";
            // 
            // checkBoxSaveCredentials
            // 
            this.checkBoxSaveCredentials.AutoSize = true;
            this.checkBoxSaveCredentials.Checked = true;
            this.checkBoxSaveCredentials.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSaveCredentials.Location = new System.Drawing.Point(166, 182);
            this.checkBoxSaveCredentials.Name = "checkBoxSaveCredentials";
            this.checkBoxSaveCredentials.Size = new System.Drawing.Size(44, 17);
            this.checkBoxSaveCredentials.TabIndex = 17;
            this.checkBoxSaveCredentials.Text = "Yes";
            this.checkBoxSaveCredentials.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(350, 16);
            this.label3.TabIndex = 18;
            this.label3.Text = "Load the XML file and click \"Attach\" to get started!";
            // 
            // checkBoxShowPassword
            // 
            this.checkBoxShowPassword.AutoSize = true;
            this.checkBoxShowPassword.Location = new System.Drawing.Point(601, 159);
            this.checkBoxShowPassword.Name = "checkBoxShowPassword";
            this.checkBoxShowPassword.Size = new System.Drawing.Size(44, 17);
            this.checkBoxShowPassword.TabIndex = 20;
            this.checkBoxShowPassword.Text = "Yes";
            this.checkBoxShowPassword.UseVisualStyleBackColor = true;
            this.checkBoxShowPassword.CheckedChanged += new System.EventHandler(this.checkBoxShowPassword_CheckedChanged);
            // 
            // labelShowPassword
            // 
            this.labelShowPassword.AutoSize = true;
            this.labelShowPassword.Location = new System.Drawing.Point(492, 160);
            this.labelShowPassword.Name = "labelShowPassword";
            this.labelShowPassword.Size = new System.Drawing.Size(89, 13);
            this.labelShowPassword.TabIndex = 19;
            this.labelShowPassword.Text = "Show Password?";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 267);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(634, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // textBoxLocalFolderName
            // 
            this.textBoxLocalFolderName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBoxLocalFolderName.Location = new System.Drawing.Point(166, 284);
            this.textBoxLocalFolderName.Name = "textBoxLocalFolderName";
            this.textBoxLocalFolderName.ReadOnly = true;
            this.textBoxLocalFolderName.Size = new System.Drawing.Size(479, 20);
            this.textBoxLocalFolderName.TabIndex = 23;
            // 
            // buttonBrowseLocalFolder
            // 
            this.buttonBrowseLocalFolder.Location = new System.Drawing.Point(15, 284);
            this.buttonBrowseLocalFolder.Name = "buttonBrowseLocalFolder";
            this.buttonBrowseLocalFolder.Size = new System.Drawing.Size(143, 23);
            this.buttonBrowseLocalFolder.TabIndex = 24;
            this.buttonBrowseLocalFolder.Text = "Browse Local Folder";
            this.buttonBrowseLocalFolder.UseVisualStyleBackColor = true;
            this.buttonBrowseLocalFolder.Click += new System.EventHandler(this.buttonBrowseLocalFolder_Click);
            // 
            // labelInterval
            // 
            this.labelInterval.AutoSize = true;
            this.labelInterval.Location = new System.Drawing.Point(15, 319);
            this.labelInterval.Name = "labelInterval";
            this.labelInterval.Size = new System.Drawing.Size(69, 13);
            this.labelInterval.TabIndex = 25;
            this.labelInterval.Text = "Sync Interval";
            // 
            // buttonSync
            // 
            this.buttonSync.Enabled = false;
            this.buttonSync.Location = new System.Drawing.Point(15, 380);
            this.buttonSync.Name = "buttonSync";
            this.buttonSync.Size = new System.Drawing.Size(99, 23);
            this.buttonSync.TabIndex = 26;
            this.buttonSync.Text = "Start Sync";
            this.buttonSync.UseVisualStyleBackColor = true;
            this.buttonSync.Click += new System.EventHandler(this.buttonStartSync_Click);
            // 
            // trackBarSyncInterval
            // 
            this.trackBarSyncInterval.Enabled = false;
            this.trackBarSyncInterval.Location = new System.Drawing.Point(166, 316);
            this.trackBarSyncInterval.Maximum = 9;
            this.trackBarSyncInterval.Minimum = 1;
            this.trackBarSyncInterval.Name = "trackBarSyncInterval";
            this.trackBarSyncInterval.Size = new System.Drawing.Size(479, 45);
            this.trackBarSyncInterval.TabIndex = 27;
            this.trackBarSyncInterval.Value = 2;
            this.trackBarSyncInterval.Scroll += new System.EventHandler(this.trackBarSyncInterval_Scroll);
            // 
            // labelTrackBarValues
            // 
            this.labelTrackBarValues.AutoSize = true;
            this.labelTrackBarValues.Location = new System.Drawing.Point(166, 347);
            this.labelTrackBarValues.Name = "labelTrackBarValues";
            this.labelTrackBarValues.Size = new System.Drawing.Size(0, 13);
            this.labelTrackBarValues.TabIndex = 28;
            this.labelTrackBarValues.Visible = false;
            // 
            // buttonViewLog
            // 
            this.buttonViewLog.Location = new System.Drawing.Point(439, 380);
            this.buttonViewLog.Name = "buttonViewLog";
            this.buttonViewLog.Size = new System.Drawing.Size(99, 23);
            this.buttonViewLog.TabIndex = 30;
            this.buttonViewLog.Text = "View Log";
            this.buttonViewLog.UseVisualStyleBackColor = true;
            this.buttonViewLog.Click += new System.EventHandler(this.buttonViewLog_Click);
            // 
            // backgroundWorkerSync
            // 
            this.backgroundWorkerSync.WorkerSupportsCancellation = true;
            this.backgroundWorkerSync.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerSync_DoWork);
            this.backgroundWorkerSync.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerSync_RunWorkerCompleted);
            // 
            // buttonMFilesServerConfig
            // 
            this.buttonMFilesServerConfig.Location = new System.Drawing.Point(439, 241);
            this.buttonMFilesServerConfig.Name = "buttonMFilesServerConfig";
            this.buttonMFilesServerConfig.Size = new System.Drawing.Size(99, 23);
            this.buttonMFilesServerConfig.TabIndex = 31;
            this.buttonMFilesServerConfig.Text = "Configure M-Files Server";
            this.buttonMFilesServerConfig.UseVisualStyleBackColor = true;
            this.buttonMFilesServerConfig.Click += new System.EventHandler(this.buttonMFilesServerConfig_Click);
            // 
            // buttonCreateService
            // 
            this.buttonCreateService.Enabled = false;
            this.buttonCreateService.Location = new System.Drawing.Point(227, 380);
            this.buttonCreateService.Name = "buttonCreateService";
            this.buttonCreateService.Size = new System.Drawing.Size(99, 23);
            this.buttonCreateService.TabIndex = 32;
            this.buttonCreateService.Text = "Create Service";
            this.buttonCreateService.UseVisualStyleBackColor = true;
            this.buttonCreateService.Click += new System.EventHandler(this.buttonCreateService_Click);
            // 
            // buttonTestConnection
            // 
            this.buttonTestConnection.Enabled = false;
            this.buttonTestConnection.Location = new System.Drawing.Point(333, 241);
            this.buttonTestConnection.Name = "buttonTestConnection";
            this.buttonTestConnection.Size = new System.Drawing.Size(99, 23);
            this.buttonTestConnection.TabIndex = 33;
            this.buttonTestConnection.Text = "Test Connection";
            this.buttonTestConnection.UseVisualStyleBackColor = true;
            this.buttonTestConnection.Click += new System.EventHandler(this.buttonTestConnection_Click);
            // 
            // buttonSharingOptions
            // 
            this.buttonSharingOptions.Location = new System.Drawing.Point(333, 380);
            this.buttonSharingOptions.Name = "buttonSharingOptions";
            this.buttonSharingOptions.Size = new System.Drawing.Size(99, 23);
            this.buttonSharingOptions.TabIndex = 34;
            this.buttonSharingOptions.Text = "Sharing Options";
            this.buttonSharingOptions.UseVisualStyleBackColor = true;
            this.buttonSharingOptions.Click += new System.EventHandler(this.buttonSharingOptions_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipTitle = "Baystream Files";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Baystream Files";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.Click += new System.EventHandler(this.NotifyIcon1_Click);
            // 
            // buttonStopSync
            // 
            this.buttonStopSync.Enabled = false;
            this.buttonStopSync.Location = new System.Drawing.Point(120, 380);
            this.buttonStopSync.Name = "buttonStopSync";
            this.buttonStopSync.Size = new System.Drawing.Size(99, 23);
            this.buttonStopSync.TabIndex = 35;
            this.buttonStopSync.Text = "Stop Sync";
            this.buttonStopSync.UseVisualStyleBackColor = true;
            this.buttonStopSync.Click += new System.EventHandler(this.buttonStopSync_Click);
            // 
            // timerSyncInterval
            // 
            this.timerSyncInterval.Tick += new System.EventHandler(this.timerSyncInterval_Tick);
            // 
            // checkBoxVerboseMode
            // 
            this.checkBoxVerboseMode.AutoSize = true;
            this.checkBoxVerboseMode.Location = new System.Drawing.Point(601, 183);
            this.checkBoxVerboseMode.Name = "checkBoxVerboseMode";
            this.checkBoxVerboseMode.Size = new System.Drawing.Size(44, 17);
            this.checkBoxVerboseMode.TabIndex = 36;
            this.checkBoxVerboseMode.Text = "Yes";
            this.checkBoxVerboseMode.UseVisualStyleBackColor = true;
            this.checkBoxVerboseMode.CheckedChanged += new System.EventHandler(this.checkBoxVerboseMode_CheckedChanged);
            // 
            // labelVerbose
            // 
            this.labelVerbose.AutoSize = true;
            this.labelVerbose.Location = new System.Drawing.Point(495, 183);
            this.labelVerbose.Name = "labelVerbose";
            this.labelVerbose.Size = new System.Drawing.Size(81, 13);
            this.labelVerbose.TabIndex = 37;
            this.labelVerbose.Text = "Verbose mode?";
            // 
            // BaystreamFilesMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 415);
            this.Controls.Add(this.labelVerbose);
            this.Controls.Add(this.checkBoxVerboseMode);
            this.Controls.Add(this.buttonStopSync);
            this.Controls.Add(this.buttonSharingOptions);
            this.Controls.Add(this.buttonTestConnection);
            this.Controls.Add(this.buttonCreateService);
            this.Controls.Add(this.buttonMFilesServerConfig);
            this.Controls.Add(this.buttonViewLog);
            this.Controls.Add(this.labelTrackBarValues);
            this.Controls.Add(this.trackBarSyncInterval);
            this.Controls.Add(this.buttonSync);
            this.Controls.Add(this.labelInterval);
            this.Controls.Add(this.buttonBrowseLocalFolder);
            this.Controls.Add(this.textBoxLocalFolderName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.checkBoxShowPassword);
            this.Controls.Add(this.labelShowPassword);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.checkBoxSaveCredentials);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.linkLabelSupport);
            this.Controls.Add(this.buttonBrowseXML);
            this.Controls.Add(this.comboBoxDriveLetters);
            this.Controls.Add(this.labelDriveLetter);
            this.Controls.Add(this.textBoxFolderName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonQuit);
            this.Controls.Add(this.labelPermanent);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxUserName);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelUsername);
            this.Controls.Add(this.textBoxServerName);
            this.Controls.Add(this.labelServerName);
            this.Controls.Add(this.checkBoxPermanent);
            this.Controls.Add(this.buttonDetach);
            this.Controls.Add(this.buttonAttach);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BaystreamFilesMainForm";
            this.Text = "Baystream FileShare Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSyncInterval)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAttach;
        private System.Windows.Forms.Button buttonDetach;
        private System.Windows.Forms.CheckBox checkBoxPermanent;
        private System.Windows.Forms.Label labelServerName;
        private System.Windows.Forms.TextBox textBoxServerName;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label labelPermanent;
        private System.Windows.Forms.Button buttonQuit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxFolderName;
        private System.Windows.Forms.Label labelDriveLetter;
        private System.Windows.Forms.ComboBox comboBoxDriveLetters;
        private System.Windows.Forms.LinkLabel linkLabelSupport;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxSaveCredentials;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxShowPassword;
        private System.Windows.Forms.Label labelShowPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxLocalFolderName;
        private System.Windows.Forms.Button buttonBrowseLocalFolder;
        private System.Windows.Forms.Label labelInterval;
        private System.Windows.Forms.TrackBar trackBarSyncInterval;
        private System.Windows.Forms.Label labelTrackBarValues;
        public System.Windows.Forms.Button buttonSync;
        private System.Windows.Forms.Button buttonViewLog;
        private System.ComponentModel.BackgroundWorker backgroundWorkerSync;
        private System.Windows.Forms.Button buttonMFilesServerConfig;
        private System.Windows.Forms.Button buttonCreateService;
        private System.Windows.Forms.Button buttonTestConnection;
        private System.Windows.Forms.Button buttonSharingOptions;
        public System.Windows.Forms.Button buttonBrowseXML;
        public System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button buttonStopSync;
        public System.Windows.Forms.Timer timerSyncInterval;
        private System.Windows.Forms.CheckBox checkBoxVerboseMode;
        private System.Windows.Forms.Label labelVerbose;
    }
}

