﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Synchronization;
using Microsoft.Synchronization.Files;


namespace BaystreamFiles
{
    public class FileSyncProvider
    {

        public static void Main(string syncLocalPath, string syncRemotePath)
        {
            //syncLocalPath,syncRemotePath
            BaystreamFilesMainForm.logWriter.LogWrite("in synhronizer class the variables are:" + syncLocalPath + " and " + syncRemotePath);


            if (syncLocalPath.Length < 2 ||
                string.IsNullOrEmpty(syncLocalPath) || string.IsNullOrEmpty(syncRemotePath) ||
                !Directory.Exists(syncLocalPath) || !Directory.Exists(syncRemotePath))
            {
                BaystreamFilesMainForm.logWriter.LogWrite("ERROR: Missing variable: [valid directory path 1] [valid directory path 2]");
                BaystreamFilesMainForm.Notifier.BalloonTipText = "ERROR: Missing variable: [valid directory path 1] [valid directory path 2]";
                BaystreamFilesMainForm.Notifier.ShowBalloonTip(2000);
                return;
            }

            string replica1RootPath = syncLocalPath;
            string replica2RootPath = syncRemotePath;

            try
            {
                // Set options for the sync operation
                FileSyncOptions options = FileSyncOptions.ExplicitDetectChanges |
                FileSyncOptions.RecycleDeletedFiles | FileSyncOptions.RecyclePreviousFileOnUpdates | FileSyncOptions.RecycleConflictLoserFiles;

                FileSyncScopeFilter filter = new FileSyncScopeFilter();
                filter.FileNameExcludes.Add("*.lnk"); // Exclude all *.lnk files
                
                // Explicitly detect changes on both replicas upfront, to avoid two change 
                // detection passes for the two-way sync
                DetectChangesOnFileSystemReplica(
                    replica1RootPath, filter, options);
                DetectChangesOnFileSystemReplica(
                    replica2RootPath, filter, options);

                // Sync in both directions
                SyncFileSystemReplicasOneWay(replica1RootPath, replica2RootPath, null, options);
                SyncFileSystemReplicasOneWay(replica2RootPath, replica1RootPath, null, options);
            }
            catch (Exception e)
            {
                BaystreamFilesMainForm.logWriter.LogWrite("Exception from File Sync Provider:\n" + e.ToString());
            }
        }

        public static void DetectChangesOnFileSystemReplica(
                string replicaRootPath,
                FileSyncScopeFilter filter, FileSyncOptions options)
        {
            Microsoft.Synchronization.Files.FileSyncProvider provider = null;

            try
            {
                provider = new Microsoft.Synchronization.Files.FileSyncProvider(replicaRootPath, filter, options);
                provider.DetectChanges();
            }
            finally
            {
                // Release resources
                if (provider != null)
                    provider.Dispose();
            }
        }

        public static void SyncFileSystemReplicasOneWay(
                string sourceReplicaRootPath, string destinationReplicaRootPath,
                FileSyncScopeFilter filter, FileSyncOptions options)
        {
            Microsoft.Synchronization.Files.FileSyncProvider sourceProvider = null;
            Microsoft.Synchronization.Files.FileSyncProvider destinationProvider = null;

            try
            {
                sourceProvider = new Microsoft.Synchronization.Files.FileSyncProvider(
                    sourceReplicaRootPath, filter, options);
                destinationProvider = new Microsoft.Synchronization.Files.FileSyncProvider(
                    destinationReplicaRootPath, filter, options);

                destinationProvider.AppliedChange +=
                    new EventHandler<AppliedChangeEventArgs>(OnAppliedChange);
                destinationProvider.SkippedChange +=
                    new EventHandler<SkippedChangeEventArgs>(OnSkippedChange);

                SyncOrchestrator agent = new SyncOrchestrator();
                agent.LocalProvider = sourceProvider;
                agent.RemoteProvider = destinationProvider;
                agent.Direction = SyncDirectionOrder.Upload; // Sync source to destination

                BaystreamFilesMainForm.logWriter.LogWrite("Synchronizing changes to replica: " + destinationProvider.RootDirectoryPath);
                agent.Synchronize();
            }
            finally
            {
                // Release resources
                if (sourceProvider != null) sourceProvider.Dispose();
                if (destinationProvider != null) destinationProvider.Dispose();
            }
        }

        public static void OnAppliedChange(object sender, AppliedChangeEventArgs args)
        {
            switch (args.ChangeType)
            {
                case ChangeType.Create:
                    BaystreamFilesMainForm.logWriter.LogWrite("-- Applied CREATE for file " + args.NewFilePath);
                    //BaystreamFilesMainForm.Notifier.BalloonTipText = "--Applied CREATE for file " + args.NewFilePath;
                    //BaystreamFilesMainForm.Notifier.ShowBalloonTip(2000);
                    break;
                case ChangeType.Delete:
                    BaystreamFilesMainForm.logWriter.LogWrite("-- Applied DELETE for file " + args.OldFilePath);
                    //BaystreamFilesMainForm.Notifier.BalloonTipText = "--Applied DELETE for file " + args.NewFilePath;
                    //BaystreamFilesMainForm.Notifier.ShowBalloonTip(2000);
                    break;
                case ChangeType.Update:
                    BaystreamFilesMainForm.logWriter.LogWrite("-- Applied OVERWRITE for file " + args.OldFilePath);
                    //BaystreamFilesMainForm.Notifier.BalloonTipText = "--Applied OVERWRITE for file " + args.NewFilePath;
                    //BaystreamFilesMainForm.Notifier.ShowBalloonTip(2000);
                    break;
                case ChangeType.Rename:
                    BaystreamFilesMainForm.logWriter.LogWrite("-- Applied RENAME for file " + args.OldFilePath + " as " + args.NewFilePath);
                    //BaystreamFilesMainForm.Notifier.BalloonTipText = "--Applied RENAME for file " + args.NewFilePath;
                    //BaystreamFilesMainForm.Notifier.ShowBalloonTip(2000);
                    break;
            }
        }

        public static void OnSkippedChange(object sender, SkippedChangeEventArgs args)
        {
            BaystreamFilesMainForm.logWriter.LogWrite("-- Skipped applying " + args.ChangeType.ToString().ToUpper()
                  + " for " + (!string.IsNullOrEmpty(args.CurrentFilePath) ?
                                args.CurrentFilePath : args.NewFilePath) + " due to error");
            BaystreamFilesMainForm.Notifier.BalloonTipText = "-- Skipped applying " + args.ChangeType.ToString().ToUpper()
                  + " for " + (!string.IsNullOrEmpty(args.CurrentFilePath) ?
                                args.CurrentFilePath : args.NewFilePath) + " due to error";
            BaystreamFilesMainForm.Notifier.ShowBalloonTip(2000);

            if (args.Exception != null)
                BaystreamFilesMainForm.logWriter.LogWrite("ERROR: [" + args.Exception.Message + "]");
                BaystreamFilesMainForm.Notifier.BalloonTipText = "ERROR: [" + args.NewFilePath + "]";
                BaystreamFilesMainForm.Notifier.ShowBalloonTip(2000);
        }
    }
}