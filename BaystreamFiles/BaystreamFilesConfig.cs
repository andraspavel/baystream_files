﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace BaystreamFiles
{
    [XmlRoot("BaystreamFilesConfiguration")]
    [Serializable]


    public class BaystreamFilesConfigs 
    {
        [XmlElement] public static string ConfigFilePath = System.Windows.Forms.Application.StartupPath +  "\\BaystreamFiles.config";
        [XmlElement] public string ConfigurationXMLfile { get; set; }
        [XmlElement] public string DestinationServerName { get; set; }
        [XmlElement] public string DestinationHotFolderName { get; set; }
        [XmlElement] public string DestinationUserName { get; set; }
        [XmlElement] public string DestinationPassword { get; set; }
        [XmlElement] public string DestinationDriveLetter { get; set; }
        [XmlElement] public bool DestinationIsPermanent { get; set; }
        [XmlElement] public bool DestinationIsCredentialsSaved { get; set; }
        [XmlElement] public bool DestinationIsPasswordShow { get; set; }
        [XmlElement] public string SourceLocalFolder { get; set; }
        [XmlElement] public bool SourceSyncIsEnabled { get; set; }
        [XmlElement] public bool SourceSyncIsStarted { get; set; }
        [XmlElement] public int DestinationSyncInterval { get; set; }
        //[XmlElement] public bool RunAsService { get; set; }
        [XmlElement] public bool VerboseMode { get; set; }
        [XmlElement] public Guid guid { get; set; }
    }
    public class BaystreamFilesConfigurator
        {
        public static BaystreamFilesConfigs BSFConfigonLoad = new BaystreamFilesConfigs();
        public void BaystreamFilesConfigLoad()
        {

            //BaystreamFilesConfigurator privateBSFConfig = new BaystreamFilesConfigurator();
            FileInfo fi = new FileInfo(BaystreamFilesConfigs.ConfigFilePath);
            if (fi.Exists)
            {
                XmlSerializer mySerializer = new XmlSerializer(BaystreamFilesMainForm.BSFConfig.GetType());
                StreamReader configXmlReader = new StreamReader(BaystreamFilesConfigs.ConfigFilePath);
                try
                {

                    BSFConfigonLoad = (BaystreamFilesConfigs)mySerializer.Deserialize(configXmlReader);
                    configXmlReader.Close();
                    BaystreamFilesMainForm.logWriter.LogWrite("Reading from Configuration XML file!");
                }
                catch (Exception e)
                {
                    BaystreamFilesMainForm.logWriter.LogWrite("Cannot load the Configuration XML file!" + e.Message + e.StackTrace + e.Data);
                }

                finally
                {
                    configXmlReader.Dispose();
                }
            }
        }

       public void BaystreamFilesConfigSave()
        {
            //BaystreamFilesConfigurator privateBSFConfig = new BaystreamFilesConfigurator();
            
            XmlSerializer mySerializer = new XmlSerializer(BaystreamFilesMainForm.BSFConfig.GetType());
            StreamWriter configXmlWriter = new StreamWriter(BaystreamFilesConfigs.ConfigFilePath);
            try
            {
                mySerializer.Serialize(configXmlWriter, BaystreamFilesMainForm.BSFConfig);
                BaystreamFilesMainForm.logWriter.LogWrite("Writing to Configuration XML file!");
            }
            catch (Exception e)
            {
                
                BaystreamFilesMainForm.logWriter.LogWrite("Cannot write the Configuration XML file!" + e.Message);
            }

            finally
            {
                configXmlWriter.Dispose();
            }
        }

    }
}
