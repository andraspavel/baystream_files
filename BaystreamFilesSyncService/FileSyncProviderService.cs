﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Synchronization;
using Microsoft.Synchronization.Files;
using BaystreamFilesSyncService;
using BaystreamFiles;
using System.Windows.Forms;

namespace BaystreamFilesSyncService
{
    public class FileSyncProviderService

    {
        //static string metadataDirectoryPath = BaystreamFilesSyncService.syncLocalPath;
        static string metadataDirectoryPath0 = Application.StartupPath;
        static string metadataDirectoryPath1 = BaystreamFilesSyncService.syncLocalPath;
        static string metadataDirectoryPath2 = BaystreamFilesSyncService.syncRemotePath;
        //static string metadataFileName0 = "filesync-0-" + BaystreamFilesSyncService.guid.ToString() + ".metadata";
        static string metadataFileName1 = "filesync-1-" + BaystreamFilesSyncService.guid.ToString() + ".metadata";
        static string metadataFileName2 = "filesync-2-" + BaystreamFilesSyncService.guid.ToString() + ".metadata";
        static string tempDirectoryPath = Path.GetTempPath();
        //static string pathToSaveConflicts = Application.StartupPath + "\\conflicts";
        static Guid SyncGUID1 = Guid.NewGuid();
        static Guid SyncGUID2 = Guid.NewGuid();

        public static void Main(string SyncServiceLocalPath, string SyncServiceRemotePath)
        {
            //syncLocalPath,syncRemotePath

            //SyncServiceRemotePath = SyncServiceRemotePath + "\\";

            
            if (BaystreamFilesSyncService.VerboseMode)
            { 

                BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: In SynhronizerService the path variables are: " + SyncServiceLocalPath + " and " + SyncServiceRemotePath);

                //Tests on paths
                if (SyncServiceLocalPath.Length < 2)
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceLocalPath is: " + SyncServiceLocalPath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceLocalPath.Length < 2 is true");
                }
                else
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceLocalPath is: " + SyncServiceLocalPath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceLocalPath.Length < 2 is false");
                }

                if (string.IsNullOrEmpty(SyncServiceLocalPath))
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceLocalPath is: " + SyncServiceLocalPath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: string.IsNullOrEmpty(SyncServiceLocalPath) is true");
                }
                else
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceLocalPath is: " + SyncServiceLocalPath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: string.IsNullOrEmpty(SyncServiceLocalPath) is false");
                }

                if (string.IsNullOrEmpty(SyncServiceRemotePath))
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceRemotePath is: " + SyncServiceRemotePath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: string.IsNullOrEmpty(SyncServiceRemotePath) is true");
                }
                else
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceLocalPath is: " + SyncServiceLocalPath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: string.IsNullOrEmpty(SyncServiceRemotePath) is false");
                }

                if (!Directory.Exists(SyncServiceLocalPath))
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceLocalPath is: " + SyncServiceLocalPath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: !Directory.Exists(SyncServiceLocalPath) is true");
                }
                else
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceLocalPath is: " + SyncServiceLocalPath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: !Directory.Exists(SyncServiceLocalPath) is false");
                }

                if (!Directory.Exists(SyncServiceRemotePath))
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceRemotePath is: " + SyncServiceRemotePath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: !Directory.Exists(SyncServiceRemotePath) is true");

                }
                else
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: SyncServiceRemotePath is: " + SyncServiceRemotePath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: !Directory.Exists(SyncServiceRemotePath) is false");
                }
            }
            /*
            if (Directory.Exists(pathToSaveConflicts))
            {
                if (BaystreamFilesSyncService.VerboseMode)
                {
                    BaystreamFilesSyncService.logWriter.LogWrite(String.Format("VERBOSE: The conflicts folder on [{0}] path is available", pathToSaveConflicts));

                    
                        string[] localFiles = Directory.GetFiles(pathToSaveConflicts);

                        foreach (var item in localFiles)
                        {
                        BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: The conflicts content is: " + item.ToString());
                        }

                        if (Directory.Exists(pathToSaveConflicts))
                        {
                        BaystreamFilesSyncService.logWriter.LogWrite(String.Format("VERBOSE: The conflicts folder on [{0}] path is available", pathToSaveConflicts));
                        }
                        else
                        {
                        BaystreamFilesSyncService.logWriter.LogWrite(String.Format("ERROR: The conflicts folder [{0}] path is not available! Please check configuration!", pathToSaveConflicts));
                        }
                        

                }
            }
            else
            {
                if (BaystreamFilesSyncService.VerboseMode)
                {
                    BaystreamFilesSyncService.logWriter.LogWrite(String.Format("WARNING: The conflicts folder on [{0}] path is not available! Please check configuration!", pathToSaveConflicts));
                }

                try
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: The conflicts folder on [{0}] is not exist. Creating: " + pathToSaveConflicts);
                    Directory.CreateDirectory(pathToSaveConflicts);
                }
                catch (Exception e)
                {

                    BaystreamFilesSyncService.logWriter.LogWrite("ERROR: Failed to create the local folder! [" + pathToSaveConflicts + "] Error message: " + e.Message);
                   
                }
            }
            */



            try
            {
                if (SyncServiceLocalPath.Length < 2 ||
                    string.IsNullOrEmpty(SyncServiceLocalPath) || string.IsNullOrEmpty(SyncServiceRemotePath) ||
                    !Directory.Exists(SyncServiceLocalPath) || !Directory.Exists(SyncServiceRemotePath))
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("INFO: FileSyncProviderService SyncServiceLocalPath is: " + SyncServiceLocalPath);
                    BaystreamFilesSyncService.logWriter.LogWrite("INFO: FileSyncProviderService SyncServiceRemotePath is: " + SyncServiceRemotePath);
                    BaystreamFilesSyncService.logWriter.LogWrite("ERROR: Missing variable: [valid directory path 1] [valid directory path 2]");
                    return;
                }
            }
            catch (Exception ex) {
                BaystreamFilesSyncService.logWriter.LogWrite("ERROR: Exception happened! Error message is: " + ex.Message);
            }

            string replica1RootPath = SyncServiceLocalPath;
            string replica2RootPath = SyncServiceRemotePath;



            try
            {
                // Set options for the sync operation
                FileSyncOptions options = FileSyncOptions.ExplicitDetectChanges |
                FileSyncOptions.RecycleDeletedFiles | FileSyncOptions.RecyclePreviousFileOnUpdates | FileSyncOptions.RecycleConflictLoserFiles;
                

                FileSyncScopeFilter filter = new FileSyncScopeFilter();
                filter.FileNameExcludes.Add("*.lnk"); // Exclude all *.lnk files
                filter.FileNameExcludes.Add("*.metadata"); // Exclude all *.metadata files
                filter.FileNameExcludes.Add("*.exe"); // Exclude all *.exe files
                filter.FileNameExcludes.Add("*.tmp"); // Exclude all *.tmp files


                if (BaystreamFilesSyncService.VerboseMode)
                {
                    foreach (var item in filter.FileNameExcludes)
                    {
                        BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: Excluded file name(s): " + item.ToString());
                    }
                }
                    
                
                // Explicitly detect changes on both replicas upfront, to avoid two change 
                // detection passes for the two-way sync
                DetectChangesOnFileSystemReplica(
                    replica1RootPath, filter, options, metadataDirectoryPath1, metadataFileName1, tempDirectoryPath, null);
                    //BaystreamFilesSyncService.guid, replica1RootPath, filter, options, metadataDirectoryPath0, metadataFileName1, tempDirectoryPath, null);
                DetectChangesOnFileSystemReplica(
                    replica2RootPath, filter, options, metadataDirectoryPath2, metadataFileName2, tempDirectoryPath, null);
                    //BaystreamFilesSyncService.guid, replica2RootPath, filter, options, metadataDirectoryPath0, metadataFileName1, tempDirectoryPath, null);

                // Sync in both directions
                //SyncFileSystemReplicasOneWay(Guid.NewGuid(), replica1RootPath, replica2RootPath, filter, options, metadataDirectoryPath0, metadataFileName1, tempDirectoryPath, null);
                //SyncFileSystemReplicasOneWay(Guid.NewGuid(), replica2RootPath, replica1RootPath, filter, options, metadataDirectoryPath0, metadataFileName1, tempDirectoryPath, null);
                SyncFileSystemReplicasOneWay(replica1RootPath, replica2RootPath, filter, options, metadataDirectoryPath1, metadataFileName1, metadataDirectoryPath2, metadataFileName2, tempDirectoryPath, null);
                SyncFileSystemReplicasOneWay(replica2RootPath, replica1RootPath, filter, options, metadataDirectoryPath2, metadataFileName2, metadataDirectoryPath1, metadataFileName1, tempDirectoryPath, null);
                //SyncFileSystemReplicasOneWay(BaystreamFilesSyncService.guid, replica1RootPath, replica2RootPath, filter, options, metadataDirectoryPath0, metadataFileName1, tempDirectoryPath, null);
                //SyncFileSystemReplicasOneWay(BaystreamFilesSyncService.guid, replica2RootPath, replica1RootPath, filter, options, metadataDirectoryPath0, metadataFileName1, tempDirectoryPath, null);
            }
            catch (Exception e)
            {
                BaystreamFilesSyncService.logWriter.LogWrite("ERROR: Exception from File Sync Provider:\n" + e.ToString());
                
            }
        }

        public static void DetectChangesOnFileSystemReplica(
                //Guid guid,
                string replicaRootPath,
                FileSyncScopeFilter filter, 
                FileSyncOptions options,
                string metadataDirectoryPath,
                string metadataFileName,
                string tempDirectoryPath,
                string pathToSaveConflictsFolder)
        {
            Microsoft.Synchronization.Files.FileSyncProvider provider = null;

            try
            {
                //provider = new Microsoft.Synchronization.Files.FileSyncProvider(replicaRootPath, filter, options);
                //provider = new Microsoft.Synchronization.Files.FileSyncProvider(Guid.NewGuid(), replicaRootPath, filter, options, metadataDirectoryPath, metadataFileName0, tempDirectoryPath, null);
                provider = new Microsoft.Synchronization.Files.FileSyncProvider(replicaRootPath, filter, options, metadataDirectoryPath, metadataFileName, tempDirectoryPath, pathToSaveConflictsFolder); // ); //
                provider.DetectChanges();
            }
            finally
            {
                // Release resources
                if (provider != null)
                    provider.Dispose();
                //DeleteMetadata(metadataFileName);
            }
        }

        public static void SyncFileSystemReplicasOneWay(
                //Guid guid,
                string sourceReplicaRootPath, 
                string destinationReplicaRootPath,
                FileSyncScopeFilter filter, 
                FileSyncOptions options, 
                string sourceMetadataDirectoryPath, 
                string sourceMetadataFileName,
                string destinationMetadataDirectoryPath,
                string destinationMetadataFileName,
                string tempDirectoryPath, 
                string pathToSaveConflictsFolder)
        {
            Microsoft.Synchronization.Files.FileSyncProvider sourceProvider = null;
            Microsoft.Synchronization.Files.FileSyncProvider destinationProvider = null;

            try
            {
                if (BaystreamFilesSyncService.VerboseMode)
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: sourceMetadataDirectoryPath is: " + sourceMetadataDirectoryPath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: sourceMetadataFileName is: " + sourceMetadataFileName);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: destinationMetadataDirectoryPath is: " + destinationMetadataDirectoryPath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: destinationMetadataFileName is: " + destinationMetadataFileName);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: metadataDirectoryPath1 is: " + metadataDirectoryPath1);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: metadataDirectoryPath2 is: " + metadataDirectoryPath2);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: metadataFileName1 is: " + metadataFileName1);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: metadataFileName2 is: " + metadataFileName2);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: tempDirectoryPath is: " + tempDirectoryPath);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: #### Local Path item count: " + Directory.GetFiles(BaystreamFilesSyncService.syncLocalPath).Length);
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: #### Remote Path item count: " + Directory.GetFiles(BaystreamFilesSyncService.syncRemotePath).Length);
                }
                
                //sourceProvider = new Microsoft.Synchronization.Files.FileSyncProvider(Guid.NewGuid(), sourceReplicaRootPath, filter, options, metadataDirectoryPath1, metadataFileName0, tempDirectoryPath, null); // ); //
                //sourceProvider = new Microsoft.Synchronization.Files.FileSyncProvider(guid, sourceReplicaRootPath, filter, options, metadataDirectoryPath, metadataFileName, tempDirectoryPath, pathToSaveConflictsFolder); // ); //
                sourceProvider = new Microsoft.Synchronization.Files.FileSyncProvider(sourceReplicaRootPath, filter, options, sourceMetadataDirectoryPath, sourceMetadataFileName, tempDirectoryPath, pathToSaveConflictsFolder); // ); //
                //sourceProvider = new Microsoft.Synchronization.Files.FileSyncProvider(sourceReplicaRootPath, filter, options); // ); //

                
                //destinationProvider = new Microsoft.Synchronization.Files.FileSyncProvider(Guid.NewGuid(), destinationReplicaRootPath, filter, options, metadataDirectoryPath1, metadataFileName1, tempDirectoryPath, null); //); //
                //destinationProvider = new Microsoft.Synchronization.Files.FileSyncProvider(guid, destinationReplicaRootPath, filter, options, metadataDirectoryPath, metadataFileName, tempDirectoryPath, pathToSaveConflictsFolder); //); //
                destinationProvider = new Microsoft.Synchronization.Files.FileSyncProvider(destinationReplicaRootPath, filter, options, destinationMetadataDirectoryPath, destinationMetadataFileName, tempDirectoryPath, pathToSaveConflictsFolder); //); //
                //destinationProvider = new Microsoft.Synchronization.Files.FileSyncProvider(destinationReplicaRootPath, filter, options); //); //

                destinationProvider.AppliedChange +=
                    new EventHandler<AppliedChangeEventArgs>(OnAppliedChange);
                destinationProvider.SkippedChange +=
                    new EventHandler<SkippedChangeEventArgs>(OnSkippedChange);

                SyncOrchestrator agent = new SyncOrchestrator();
                agent.LocalProvider = sourceProvider;
                agent.RemoteProvider = destinationProvider;
                agent.Direction = SyncDirectionOrder.Upload; // Sync source to destination
                
                if (BaystreamFilesSyncService.VerboseMode)
                {
                    BaystreamFilesSyncService.logWriter.LogWrite("VERBOSE: Synchronizing changes to replica: " + destinationProvider.RootDirectoryPath);
                }
                agent.Synchronize();
            }
            finally
            {
                // Release resources
                if (sourceProvider != null) sourceProvider.Dispose();
                if (destinationProvider != null) destinationProvider.Dispose();
                //DeleteMetadata(metadataFileName);
            }
        }

        public static void OnAppliedChange(object sender, AppliedChangeEventArgs args)
        {
            switch (args.ChangeType)
            {
                case ChangeType.Create:
                    BaystreamFilesSyncService.logWriter.LogWrite("INFO: FileOperation: Applied CREATE for file " + args.NewFilePath);
                    //BaystreamFilesMainForm.Notifier.BalloonTipText = "--Applied CREATE for file " + args.NewFilePath;
                    //BaystreamFilesMainForm.Notifier.ShowBalloonTip(2000);
                    break;
                case ChangeType.Delete:
                    BaystreamFilesSyncService.logWriter.LogWrite("INFO: FileOperation: Applied DELETE for file " + args.OldFilePath);
                    //BaystreamFilesMainForm.Notifier.BalloonTipText = "--Applied DELETE for file " + args.NewFilePath;
                    //BaystreamFilesMainForm.Notifier.ShowBalloonTip(2000);
                    break;
                case ChangeType.Update:
                    BaystreamFilesSyncService.logWriter.LogWrite("INFO: FileOperation: Applied OVERWRITE for file " + args.OldFilePath);
                    //BaystreamFilesMainForm.Notifier.BalloonTipText = "--Applied OVERWRITE for file " + args.NewFilePath;
                    //BaystreamFilesMainForm.Notifier.ShowBalloonTip(2000);
                    break;
                case ChangeType.Rename:
                    BaystreamFilesSyncService.logWriter.LogWrite("INFO: FileOperation: Applied RENAME for file " + args.OldFilePath + " as " + args.NewFilePath);
                    //BaystreamFilesMainForm.Notifier.BalloonTipText = "--Applied RENAME for file " + args.NewFilePath;
                    //BaystreamFilesMainForm.Notifier.ShowBalloonTip(2000);
                    break;
            }
        }

        public static void OnSkippedChange(object sender, SkippedChangeEventArgs args)
        {
            BaystreamFilesSyncService.logWriter.LogWrite("INFO: FileOperation: Skipped applying " + args.ChangeType.ToString().ToUpper()
                  + " for " + (!string.IsNullOrEmpty(args.CurrentFilePath) ?
                                args.CurrentFilePath : args.NewFilePath) + " due to error");
            
            if (args.Exception != null)
                BaystreamFilesSyncService.logWriter.LogWrite("ERROR: [" + args.Exception.Message + "]");
        }
        public static void DeleteMetadata(string FilePath)
        {
            File.Delete(FilePath);
            BaystreamFilesSyncService.logWriter.LogWrite("### Deleting [" + FilePath + "] MetaData file.");
        }
    }
}