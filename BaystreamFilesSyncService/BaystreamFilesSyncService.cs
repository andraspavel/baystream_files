﻿using BaystreamFiles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Collections;
using System.IO;
using System.Reflection;

namespace BaystreamFilesSyncService
{
    public partial class BaystreamFilesSyncService : ServiceBase
    {

        //####################################
        //Creating Static objects
        public static BaystreamFilesConfigurator BSFConfigurator = new BaystreamFilesConfigurator();
        public static BaystreamFilesConfigs BSFConfig = new BaystreamFilesConfigs();
        public static LogWriter logWriter = new LogWriter("\n\n#######################################################################################\n#######################################################################################\nStarting appplication in Service Mode..\n");
        


        //Declaring variables:
        public static string ServerName;
        public static string FolderName;
        public static string UserName;
        public static string Password;
        public static string encryptedPassword;
        public static string LocalDrive;
        public static string SyncDrive;
        public static string SourceLocalFolder;
        public static string ShareNameString;
        public static string syncLocalPath;
        public static string syncRemotePath;
        public static ArrayList AvailableDriveLetters;
        public static int syncInterval;
        public static int syncIntervalMs;
        public static bool syncIsEnabled;
        public static string configurationReceivedXMLFilePath;
        public static bool isSyncStarted;
        public static bool RunAsService;
        public static bool VerboseMode;
        public static Guid guid;

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);


        public BaystreamFilesSyncService()
        {
            InitializeComponent();
        }

        private void RegisterDLLs()
        {
            //Microsoft.Synchronization.dll
            logWriter.LogWrite("Registering Microsoft.Synchronization.dll");
            Process regAsmProcess = new Process();
            ProcessStartInfo Process_Info = new ProcessStartInfo();
            Process_Info.FileName = "C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\Regasm.exe";
            Process_Info.Arguments = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Microsoft.Synchronization.dll /tlb";
            //Process_Info.Verb = "runas";
            regAsmProcess.StartInfo = Process_Info;
            regAsmProcess.Start();

            //Microsoft.Synchronization.Files.dll
            logWriter.LogWrite("Registering Microsoft.Synchronization.Files.dll");
            Process regAsmProcess2 = new Process();
            ProcessStartInfo Process_Info2 = new ProcessStartInfo();
            Process_Info2.FileName = "C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\Regasm.exe";
            Process_Info2.Arguments = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Microsoft.Synchronization.Files.dll /tlb";
            //Process_Info.Verb = "runas";
            regAsmProcess2.StartInfo = Process_Info2;
            regAsmProcess2.Start();

            /*
            //Microsoft.Synchronization.dll
            logWriter.LogWrite("Registering Microsoft.Synchronization.dll - x64");
            Process regAsmProcess3 = new Process();
            ProcessStartInfo Process_Info3 = new ProcessStartInfo();
            Process_Info3.FileName = "C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\Regasm.exe";
            Process_Info3.Arguments = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Microsoft.Synchronization.dll /tlb";
            //Process_Info.Verb = "runas";
            regAsmProcess3.StartInfo = Process_Info3;
            regAsmProcess3.Start();

            //Microsoft.Synchronization.Files.dll
            logWriter.LogWrite("Registering Microsoft.Synchronization.Files.dll - x64");
            Process regAsmProcess4 = new Process();
            ProcessStartInfo Process_Info4 = new ProcessStartInfo();
            Process_Info4.FileName = "C:\\Windows\\Microsoft.NET\\Framework\\v4.0.30319\\Regasm.exe";
            Process_Info4.Arguments = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Microsoft.Synchronization.Files.dll /tlb";
            //Process_Info.Verb = "runas";
            regAsmProcess4.StartInfo = Process_Info4;
            regAsmProcess4.Start();
            */
            /*
            string cmdArgument = String.Format("\"/c net use {0} {1} /u:{2} {3} /persistent:yes", SyncDrive, ShareNameString, UserName, Password);
            logWriter.LogWrite(string.Format("cmdArgument is: " + cmdArgument));
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            //MessageBox.Show(string.Format("arguments are {0}", cmdArgument), "Results", MessageBoxButtons.OK);
            startInfo.Arguments = string.Format("{0}", cmdArgument);
            process.StartInfo = startInfo;
            process.Start();
            logWriter.LogWrite(String.Format("The {0} path was successfully attached to {1} drive.", ShareNameString, SyncDrive));
            */
        }


        private void DriveLetters()
        {

            logWriter.LogWrite("Retreiving free drive letters");
            ArrayList driveLetters = new ArrayList(26); // Allocate space for alphabet
            for (int i = 65; i < 91; i++) // increment from ASCII values for A-Z
            {
                driveLetters.Add(Convert.ToChar(i)); // Add uppercase letters to possible drive letters
            }

            foreach (string drive in Directory.GetLogicalDrives())
            {
                driveLetters.Remove(drive[0]); // removed used drive letters from possible drive letters
                logWriter.LogWrite("The " + drive[0] + " is not a valid drive, removing it from list");
            }

            AvailableDriveLetters = driveLetters;

            foreach (char drive in AvailableDriveLetters)
            {
                logWriter.LogWrite("The " + drive + " drive is available");
            }
            //Prefer last valid drive
            AvailableDriveLetters.Reverse();
            SyncDrive = AvailableDriveLetters[0].ToString() + ":";
            logWriter.LogWrite("INFO: The SyncDrive is " + SyncDrive);
        }

        private void AddNetworkDrive() {
            ShareNameString = String.Format("\\\\{0}\\{1}", ServerName, FolderName);
            //LocalDrive = AvailableDriveLetter[0].ToString() + ":";
            //MessageBox.Show(String.Format("\"{0}\", \"{1}\"", UserName, Password));
            //MessageBox.Show(ShareNameString.ToString());
            //net use * \\hvfiles-01.baymain.com\testfolder11 /u:testuser11 P@ssw0rd01! /persistent:yes
            //string cmdArgument = String.Format("\"/c net use {0} {1} /u:{2} {3} /persistent:yes", SyncDrive, ShareNameString, UserName, Password);
            string cmdArgument = String.Format("\"/c net use {0} /u:{1} {2}", ShareNameString, UserName, Password);
            logWriter.LogWrite(string.Format("cmdArgument is: " + cmdArgument));
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            //MessageBox.Show(string.Format("arguments are {0}", cmdArgument), "Results", MessageBoxButtons.OK);
            startInfo.Arguments = string.Format("{0}", cmdArgument);
            process.StartInfo = startInfo;
            process.Start();

            Thread.Sleep(1000);

            
            if (VerboseMode)
            {
                /*
                string[] remoteFiles = Directory.GetFiles(ShareNameString);

                foreach (var item in remoteFiles)
                {
                    logWriter.LogWrite("Remote path content is: " + item.ToString());
                }
                */
                if (Directory.Exists(ShareNameString))
                {
                    logWriter.LogWrite(String.Format("INFO: The {0} path is available", ShareNameString));
                }
                else
                {
                    logWriter.LogWrite(String.Format("ERROR: The {0} path is not available! Please check configuration!", ShareNameString));
                    logWriter.LogWrite("ERROR: Stopping service!");
                    OnStop();
                    
                }
            }
           

        }

        private void LocalPath()
        {
            if (Directory.Exists(SourceLocalFolder))
            {
                if (VerboseMode)
                {
                    logWriter.LogWrite(String.Format("INFO: The local {0} path is available", SourceLocalFolder));

                    /*
                        string[] localFiles = Directory.GetFiles(ShareNameString);

                        foreach (var item in localFiles)
                        {
                            logWriter.LogWrite("Local folder content is: " + item.ToString());
                        }

                        if (Directory.Exists(SourceLocalFolder))
                        {
                            logWriter.LogWrite(String.Format("INFO: The {0} path is available", SourceLocalFolder));
                        }
                        else
                        {
                            logWriter.LogWrite(String.Format("ERROR: The {0} path is not available! Please check configuration!", SourceLocalFolder));
                            logWriter.LogWrite("ERROR: Stopping service!");
                            OnStop();

                        }
                        */

                }
            }
            else
            {
                if (VerboseMode)
                {
                    logWriter.LogWrite(String.Format("WARNING: The local {0} path is not available! Please check configuration!", SourceLocalFolder));
                }

                try
                {
                    logWriter.LogWrite("INFO: Local folder is not exist. Creating: " + SourceLocalFolder);
                    Directory.CreateDirectory(SourceLocalFolder);
                }
                catch (Exception e)
                {

                    logWriter.LogWrite("ERROR: Failed to create the local folder! [" + SourceLocalFolder + "] Error message: " + e.Message);
                    OnStop();
                    base.Stop();
                }
            }
        }

        private void DeleteNetworkDrive()
        {
            ShareNameString = String.Format("\\\\{0}\\{1}", ServerName, FolderName);
            //MessageBox.Show(String.Format("\"{0}\", \"{1}\"", UserName, Password));
            //MessageBox.Show(ShareNameString.ToString());
            //net use * \\hvfiles-01.baymain.com\testfolder11 /u:testuser11 P@ssw0rd01!
            string cmdArgument = String.Format("\"/c net use {0} /delete", SyncDrive);
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            //MessageBox.Show(string.Format("arguments are {0}", cmdArgument), "Results", MessageBoxButtons.OK);
            startInfo.Arguments = string.Format("{0}", cmdArgument);
            process.StartInfo = startInfo;
            process.Start();
            logWriter.LogWrite(String.Format("The {0} path was successfully detached from {1} drive.", ShareNameString, SyncDrive));
        }

        
        private void AddCMDKey(string localServerName, string localUserName, string localPassword) {


            System.Diagnostics.Process processCMDKey = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfoCMDKey = new System.Diagnostics.ProcessStartInfo();
            startInfoCMDKey.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfoCMDKey.FileName = "cmd.exe";
            //cmdkey /add:targetname /user:username /pass:password
            string cmdKEYArgument = String.Format("\"/c cmdkey /add:{0} /user:{1} /pass:{2}", localServerName, localUserName, localPassword);
            startInfoCMDKey.Arguments = string.Format("{0}", cmdKEYArgument);
            processCMDKey.StartInfo = startInfoCMDKey;
            processCMDKey.Start();
            logWriter.LogWrite("The credentials saved successfully to the Service Account");
        }

        private void DeleteCMDKey(string localServerName)
        {

            System.Diagnostics.Process processCMDKey = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfoCMDKey = new System.Diagnostics.ProcessStartInfo();
            startInfoCMDKey.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfoCMDKey.FileName = "cmd.exe";
            //cmdkey /add:targetname /user:username /pass:password
            string cmdKEYArgument = String.Format("\"/c cmdkey /delete:{0}", localServerName);
            //MessageBox.Show(string.Format("arguments are {0}", cmdKEYArgument), "Results", MessageBoxButtons.OK);
            startInfoCMDKey.Arguments = string.Format("{0}", cmdKEYArgument);
            processCMDKey.StartInfo = startInfoCMDKey;
            processCMDKey.Start();
            //MessageBox.Show("The credentials were deleted successfully.");
            logWriter.LogWrite(string.Format("Stored credentials were deleted successfully for [{0}] server.", localServerName));
        }

        private void syncIntervalMsConfiguration()
        {
            switch (syncInterval)
            {
                case 1:
                    syncIntervalMs = 60000;
                    break;
                case 2:
                    syncIntervalMs = 300000;
                    break;
                case 3:
                    syncIntervalMs = 900000;
                    break;
                case 4:
                    syncIntervalMs = 1800000;
                    break;
                case 5:
                    syncIntervalMs = 3600000;
                    break;
                case 6:
                    syncIntervalMs = 7200000;
                    break;
                case 7:
                    syncIntervalMs = 14400000;
                    break;
                case 8:
                    syncIntervalMs = 28800000;
                    break;
                case 9:
                    syncIntervalMs = 86400000;
                    break;
                default:
                  
                    break;

            }
            logWriter.LogWrite("syncIntervalMs is: " + syncIntervalMs + " milliseconds.");
        }

        public void startSync()
        {
            //####################################
            //Starting AddCMDKey() method

            if (VerboseMode)
            {
                logWriter.LogWrite(string.Format("Connection credentials are - SERVERNAME: {0}, USERNAME: {1}, PASSWORD: {2}: ", ServerName, UserName, Password));
            }


            //Thread.Sleep(100);
            


            string strSyncInterval = (syncIntervalMs / 1000).ToString();
            
            syncIsEnabled = true;
            
            timerSyncInterval.Interval = syncIntervalMs;
            if (VerboseMode)
            {
                logWriter.LogWrite("strSyncInterval is: " + strSyncInterval);
                logWriter.LogWrite("Set Timer Tick Interval to [" + syncIntervalMs + "] ms");
                logWriter.LogWrite("Enable and start Timer!");
                logWriter.LogWrite("Timer Interval is: " + syncIntervalMs);
                logWriter.LogWrite("Start Timer!");
            }

            timerSyncInterval.Enabled = true;
            timerSyncInterval_Elapsed(null, null);
            

            //Start Timer
            timerSyncInterval.Start();
            logWriter.LogWrite("Sync Started..!");
            logWriter.LogWrite("Sync Interval is: " + strSyncInterval + " seconds.");
            logWriter.LogWrite("Local Synchronization Folder:" + syncLocalPath);
            logWriter.LogWrite("Remote Synchronization Folder:" + syncRemotePath);
            
        }

        
        private void timerSyncInterval_Elapsed(object sender, EventArgs e)
        {

            
            //Thread.Sleep(1000);
            timerSyncInterval.Enabled = false;
            isSyncStarted = true;
            RunAsService = true;
            logWriter.LogWrite("Tick..");
            if (!backgroundWorkerSync.IsBusy)
            {
                if (VerboseMode)
                {
                    logWriter.LogWrite("Starting Background Synchronizer Worker Thread..");
                }
                backgroundWorkerSync.RunWorkerAsync();
            }
            else
            {
                logWriter.LogWrite("Synchronization is still in progress! Waiting for next cycle..");
            }
        }

        public void stopSync()
        {
            isSyncStarted = false;
            timerSyncInterval.Stop();
            timerSyncInterval.Enabled = false;
            syncIsEnabled = false;

            backgroundWorkerSync.CancelAsync();
            backgroundWorkerSync.Dispose();
            
            //DeleteNetworkDrive();
            DeleteCMDKey(ServerName);
            Thread.Sleep(500);
        }

        private void backgroundWorkerSync_DoWork(object sender, DoWorkEventArgs e)
        {
            if (VerboseMode)
            {
                logWriter.LogWrite("Synchronization starting up..");
                logWriter.LogWrite("syncLocalPath is:" + syncLocalPath);
                logWriter.LogWrite("syncRemotePath is:" + syncRemotePath);
            }
            FileSyncProviderService.Main(syncLocalPath, syncRemotePath);

        }

        private void backgroundWorkerSync_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Check to see if the background process was cancelled.
            if (e.Cancelled == true)
            {
                syncIsEnabled = false;
                logWriter.LogWrite("Synchronization Cancelled. Finishing current executions.");
                backgroundWorkerSync.CancelAsync();
                backgroundWorkerSync.Dispose();
                isSyncStarted = false;
                timerSyncInterval.Enabled = false;

            }
            else if (e.Error != null)
            {
                logWriter.LogWrite(string.Format("ERROR in Sync Execution: " + e.Error.InnerException));
                syncIsEnabled = false;
                isSyncStarted = false;
            }
            else
            {
                syncIsEnabled = true;
                isSyncStarted = true;
                if (VerboseMode)
                {
                    logWriter.LogWrite("Restarting timer...");
                }
                timerSyncInterval.Enabled = true;
                timerSyncInterval.Start();
                


            }
        }
        protected override void OnStart(string[] args)
        {


            // Update the service state to Start Pending.  
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            //####################################
            //Registering Sync Framework DLLs
            //RegisterDLLs();


            //####################################
            //Load data from configuration file..

            BSFConfigurator.BaystreamFilesConfigLoad();

            logWriter.LogWrite("Loading data from configuration file..");


            LocalDrive = ((BaystreamFilesConfigurator.BSFConfigonLoad.DestinationDriveLetter).Length > 0) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationDriveLetter : null;
            FolderName = ((BaystreamFilesConfigurator.BSFConfigonLoad.DestinationHotFolderName).Length > 4) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationHotFolderName : null;
            encryptedPassword = ((BaystreamFilesConfigurator.BSFConfigonLoad.DestinationPassword).Length > 10) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationPassword : null;
            //Decrypt Password for service:
            Password = Encryption.Encryption.Decrypt(encryptedPassword);
            ServerName = ((BaystreamFilesConfigurator.BSFConfigonLoad.DestinationServerName).Length > 5) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationServerName : null;
            syncInterval = (BaystreamFilesConfigurator.BSFConfigonLoad.DestinationSyncInterval > 0) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationSyncInterval : 1;
            UserName = ((BaystreamFilesConfigurator.BSFConfigonLoad.DestinationUserName).Length > 3) ? BaystreamFilesConfigurator.BSFConfigonLoad.DestinationUserName : null;
            syncLocalPath = ((BaystreamFilesConfigurator.BSFConfigonLoad.SourceLocalFolder).Length > 3) ? BaystreamFilesConfigurator.BSFConfigonLoad.SourceLocalFolder : "";
            SourceLocalFolder = ((BaystreamFilesConfigurator.BSFConfigonLoad.SourceLocalFolder).Length > 0) ? BaystreamFilesConfigurator.BSFConfigonLoad.SourceLocalFolder : "";
            syncIsEnabled = (BaystreamFilesConfigurator.BSFConfigonLoad.SourceSyncIsEnabled) ? BaystreamFilesConfigurator.BSFConfigonLoad.SourceSyncIsEnabled : false;
            isSyncStarted = (BaystreamFilesConfigurator.BSFConfigonLoad.SourceSyncIsStarted) ? BaystreamFilesConfigurator.BSFConfigonLoad.SourceSyncIsStarted : false;
            //RunAsService = (BaystreamFilesConfigurator.BSFConfigonLoad.RunAsService) ? BaystreamFilesConfigurator.BSFConfigonLoad.RunAsService : false;
            VerboseMode = (BaystreamFilesConfigurator.BSFConfigonLoad.VerboseMode) ? BaystreamFilesConfigurator.BSFConfigonLoad.VerboseMode : false;

            try
            {
                guid = BaystreamFilesConfigurator.BSFConfigonLoad.guid;
                if (guid.ToString().Contains("00000000-0000-0000-0000-000000000000") || guid.ToString().Length < 10)
                {
                    logWriter.LogWrite("ERROR: Installation GUID is not valid! Stopping service.");
                    base.Stop();
                }
                else
                {

                    if (VerboseMode)
                    {
                        logWriter.LogWrite("INFO: Installation GUID is: " + guid);
                    }
                }
            }
            catch (Exception e)
            {
                logWriter.LogWrite("ERROR: Installation GUID cannot be retreived. Error Message: " + e.Message);
                base.Stop();
            }
           

            
            
            


            syncRemotePath = String.Format("\\\\{0}\\{1}", ServerName, FolderName);

            if (VerboseMode)
            {
                logWriter.LogWrite("//####################################");
                logWriter.LogWrite("//####################################");

                logWriter.LogWrite("LocalDrive is: " + LocalDrive);
                logWriter.LogWrite("FolderName is: " + FolderName);
                logWriter.LogWrite("encryptedPassword is: " + encryptedPassword);
                logWriter.LogWrite("Password is:" + Password);
                logWriter.LogWrite("ServerName is:" + ServerName);
                logWriter.LogWrite("syncInterval is:" + syncInterval);
                logWriter.LogWrite("UserName is:" + UserName);
                logWriter.LogWrite("syncLocalPath is:" + syncLocalPath);
                logWriter.LogWrite("SourceLocalFolder is:" + SourceLocalFolder);
                logWriter.LogWrite("syncIsEnabled is:" + syncIsEnabled.ToString());
                logWriter.LogWrite("isSyncStarted is:" + isSyncStarted.ToString());
                logWriter.LogWrite("RunAsService is:" + RunAsService.ToString());
                logWriter.LogWrite("VerboseMode is:" + VerboseMode.ToString());

                logWriter.LogWrite("syncRemotePath is:" + syncRemotePath.ToString());
                
                logWriter.LogWrite("//####################################");
                logWriter.LogWrite("//####################################");
            }

           
            logWriter.LogWrite("Loading data from configuration file is done.");


            if (ServerName == null || UserName == null || syncLocalPath == null || Password ==null)
            {
                logWriter.LogWrite("Configuration file is not valid! Stopping service!");
                OnStop();
            }


            //####################################
            //Starting DeleteCMDKey() method
            AddCMDKey(ServerName, UserName, Password);
            Thread.Sleep(500);

            //####################################
            //Starting syncIntervalMsConfiguration() method
            syncIntervalMsConfiguration();
            Thread.Sleep(1000);

            //####################################
            //Starting LocalPath() method
            LocalPath();
            Thread.Sleep(1000);

            //####################################
            //Starting DriveLetters() method
            //DriveLetters();
            //Thread.Sleep(1000);

            //####################################
            //Add local drive to network service.
            AddNetworkDrive();
            Thread.Sleep(1000);



            //####################################
            //Starting startSync() method
            startSync();



            //####################################
            // Update the service state to Running.  
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }


        protected override void OnStop()
        {
            logWriter.LogWrite("Stopping service. Disposing all threads..");
            stopSync();
            logWriter.LogWrite("Service stopped..");

        }


    }
    public enum ServiceState
    {
        SERVICE_STOPPED = 0x00000001,
        SERVICE_START_PENDING = 0x00000002,
        SERVICE_STOP_PENDING = 0x00000003,
        SERVICE_RUNNING = 0x00000004,
        SERVICE_CONTINUE_PENDING = 0x00000005,
        SERVICE_PAUSE_PENDING = 0x00000006,
        SERVICE_PAUSED = 0x00000007,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ServiceStatus
    {
        public long dwServiceType;
        public ServiceState dwCurrentState;
        public long dwControlsAccepted;
        public long dwWin32ExitCode;
        public long dwServiceSpecificExitCode;
        public long dwCheckPoint;
        public long dwWaitHint;
    };
}
