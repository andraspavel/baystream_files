﻿using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace BaystreamFilesSyncService
{
    partial class BaystreamFilesSyncService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerSyncInterval = new System.Timers.Timer();
            this.backgroundWorkerSync = new System.ComponentModel.BackgroundWorker();
            // 
            // timerSyncInterval
            // 
            this.timerSyncInterval.Elapsed += new System.Timers.ElapsedEventHandler(this.timerSyncInterval_Elapsed);
            // backgroundWorkerSync
            // 
            this.backgroundWorkerSync.WorkerSupportsCancellation = true;
            this.backgroundWorkerSync.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerSync_DoWork);
            this.backgroundWorkerSync.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerSync_RunWorkerCompleted);
            // 
            // BaystreamFilesSyncService
            // 
            this.ServiceName = "Baystream Files - Sync Service";
            /*
            try
            {
                //Microsoft.Synchronization.dll
                logWriter.LogWrite("INFO: Loading the Microsoft.Synchronization.dll assembly");
                string assemblyPath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                assemblyPath = assemblyPath + "\\Microsoft.Synchronization.dll";
                Assembly asm = Assembly.LoadFile(assemblyPath);
                RegistrationServices regAsm = new RegistrationServices();
                bool bResult = regAsm.RegisterAssembly(asm, AssemblyRegistrationFlags.SetCodeBase);
                logWriter.LogWrite("INFO: Result: " +bResult.ToString());

                //Microsoft.Synchronization.Files.dll
                logWriter.LogWrite("INFO: Loading the Microsoft.Synchronization.Files.dll assembly");
                string assemblyPath2 = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                assemblyPath2 = assemblyPath2 + "\\Microsoft.Synchronization.Files.dll";
                Assembly asm2 = Assembly.LoadFile(assemblyPath2);
                RegistrationServices regAsm2 = new RegistrationServices();
                regAsm2.RegisterAssembly(asm2, AssemblyRegistrationFlags.SetCodeBase);
                //bool bResult2 = regAsm2.RegisterAssembly(asm2, AssemblyRegistrationFlags.SetCodeBase);
                //logWriter.LogWrite("INFO: Result: " + bResult2.ToString());


            }
            catch (System.Exception ex)
            {
                logWriter.LogWrite("ERROR: Failed to load the necessary assemblies! Error message: " + ex.Message);
            }
            */

        }

        #endregion

        public System.Timers.Timer timerSyncInterval;
        public System.ComponentModel.BackgroundWorker backgroundWorkerSync;
    }
}
